FROM alpine:latest AS fswatch

RUN apk add --no-cache autoconf alpine-sdk

RUN rm /usr/include/sys/inotify.h
RUN wget https://github.com/emcrisostomo/fswatch/releases/download/1.17.1/fswatch-1.17.1.tar.gz     && tar -xzvf fswatch-1.17.1.tar.gz     && cd fswatch-1.17.1     && ./configure     && make     && make install     && rm -rf /fswatch-1.17.1

# Main image
FROM php:8.3-fpm-alpine

# Set build arguments
ARG USER_ID
ARG GROUP_ID
ARG USER

# Create a group and user
RUN addgroup -g ${GROUP_ID} -S ${USER} && adduser -u ${USER_ID} -S ${USER} -G ${USER}

# Set the working directory
WORKDIR /var/www/html

# Useful PHP extension installer script, copy binary into your container
COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/

# Install PHP extensions
RUN set -eux && /usr/local/bin/install-php-extensions pdo pdo_mysql amqp sockets

# Obtain Composer using multi-stage build
COPY --from=composer:2.4 /usr/bin/composer /usr/bin/composer

# Install necessary libraries for fswatch
RUN apk add --no-cache libintl libstdc++ gettext

# Copy fswatch and its libraries from the fswatch build stage
COPY --from=fswatch /usr/local/bin/fswatch /usr/local/bin/fswatch
COPY --from=fswatch /usr/local/lib/libfswatch.so* /usr/local/lib/

# Switch to the created user
USER ${USER}

