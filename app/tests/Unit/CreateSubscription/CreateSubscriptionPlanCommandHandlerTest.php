<?php
namespace Infrastructure\Tests\Unit\CreateSubscription;

use Domain\Subscription\Events\MemberJoined;
use Infrastructure\Symfony\DateProvider\FakeDateProvider;
use Infrastructure\Symfony\Uuid\FakeUUIDGenerator;
use Application\Subscription\Command\CreateSubscriptionPlanCommand;
use Infrastructure\Symfony\Plan\InMemorySubscriptionPlanRepository;
use Application\Subscription\Command\CreateSubscriptionPlanCommandHandler;

describe('CreateSubscriptionPlanCommandHandler', function () {

    it('correctly handles creating a subscription plan command', function () {
        // Arrange
        $uuidGenerator = new FakeUUIDGenerator();
        $repository = new InMemorySubscriptionPlanRepository();
        $dateProvider = new FakeDateProvider(new \DateTime('2024-05-14'));
        $command = new CreateSubscriptionPlanCommand(
            price: 99.99,
            name: 'Premium Plan',
            annualDiscount: 0.10,
            expirationDate: '2025-12-31',
            includedActivities: ['Gym', 'Swimming', 'Sauna']
        );

        $handler = new CreateSubscriptionPlanCommandHandler($repository, $uuidGenerator, $dateProvider);

        // Act
        $response = $handler->handle($command);

        // Assert
        expect($response->value)->toBe($uuidGenerator->generateId());
        expect($response->events())->toBeArray();
        expect($response->events())->toHaveCount(1);
    });

    it('updates the repository state appropriately after creating a subscription plan', function () {
        // Arrange
        $uuidGenerator = new FakeUUIDGenerator();
        $repository = new InMemorySubscriptionPlanRepository();
        $dateProvider = new FakeDateProvider(new \DateTime('2024-05-14'));
        $command = new CreateSubscriptionPlanCommand(
            price: 99.99,
            name: 'Premium Plan',
            annualDiscount: 0.10,
            expirationDate: '2025-12-31',
            includedActivities: ['Gym', 'Swimming', 'Sauna']
        );

        $handler = new CreateSubscriptionPlanCommandHandler($repository, $uuidGenerator, $dateProvider);

        // Act
        $handler->handle($command);
        $allPlans = $repository->getAll();

        // Assert
        expect($allPlans)->toBeArray();
        expect(count($allPlans))->toBe(1);
        [$addedPlan] = $allPlans;
        expect($addedPlan->snapshot())->toMatchArray([
            'id' => $uuidGenerator->generateId(),
            'name' => 'Premium Plan',
            'price' => ['amount' => 99.99, 'currency' => 'EUR'],
            'annualDiscount' => 0.10,
            'expirationDate' => '2025-12-31T00:00:00+00:00',
            'includedActivities' => ['Gym', 'Swimming', 'Sauna']
        ]);
    });

    it('correctly emits events after creating a subscription plan', function () {
        // Arrange
        $uuidGenerator = new FakeUUIDGenerator();
        $repository = new InMemorySubscriptionPlanRepository();
        $dateProvider = new FakeDateProvider(new \DateTime('2024-05-14'));
        $command = new CreateSubscriptionPlanCommand(
            price: 99.99,
            name: 'Premium Plan',
            annualDiscount: 0.10,
            expirationDate: '2025-12-31',
            includedActivities: ['Gym', 'Swimming', 'Sauna']
        );

        $handler = new CreateSubscriptionPlanCommandHandler($repository, $uuidGenerator, $dateProvider);

        // Act
        $response = $handler->handle($command);

        // Assert
        expect($response->events())->toBeArray();
        expect($response->events())->toHaveCount(1);
        /** @var MemberJoined */
        $event = current($response->events());
        expect($event)->toBeInstanceOf(MemberJoined::class);
    });
});
