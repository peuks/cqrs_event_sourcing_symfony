<?php

use Ramsey\Uuid\Uuid;
use Domain\Bank\BankAccount;
use Domain\Bank\Events\MoneyDeposited;
use Domain\Bank\InMemoryBankAccountRepository;
use Domain\Bank\Exception\AccountNotFoundException;
use Application\Bank\Command\DepositMoney\DepositMoneyCommand;
use Application\Bank\Command\DepositMoney\DepositMoneyCommandHandler;

describe('DepositMoneyCommandHandler', function () {
    describe('Happy Path', function () {
        it('correctly handles creating a deposit money command', function () {
            // Arrange
            $accountId = Uuid::uuid4();
            $amount = 100.00;

            // Create a real instance of BankAccount
            $account = new BankAccount($accountId);
            $repository = new InMemoryBankAccountRepository();
            $repository->save($account);

            $command = new DepositMoneyCommand($accountId->toString(), $amount);
            $handler = new DepositMoneyCommandHandler($repository);

            // Act
            /** @var \Application\Shared\CommandResponse $response  */
            $response = $handler->handle(command: $command);

            // Assert
            expect($response->value)->toEqual($accountId);
            expect($response->events())->toBeArray();
            expect($response->events())->toHaveCount(1);
        });

        it('correctly emits events after deposit', function () {
            // Arrange
            $accountId = Uuid::uuid4();
            $amount = 100.00;

            // Create a real instance of BankAccount
            $account = new BankAccount($accountId);
            $repository = new InMemoryBankAccountRepository();
            $repository->save($account);

            $command = new DepositMoneyCommand($accountId->toString(), $amount);
            $handler = new DepositMoneyCommandHandler($repository);

            // Act
            /** @var \Application\Shared\Contracts\CommandResponse $response  */
            $response = $handler->handle(command: $command);

            // Assert
            expect($response->events())->toBeArray();
            expect($response->events())->toHaveCount(1);
            expect(current($response->events()))->toBeInstanceOf(MoneyDeposited::class);
        });
    });

    describe('Unhappy Path', function () {
        it('throws an exception when trying to deposit a negative amount', function () {
            // Arrange
            $accountId = Uuid::uuid4();
            $amount = -100.00;

            // Create a real instance of BankAccount
            $account = new BankAccount($accountId);
            $repository = new InMemoryBankAccountRepository();
            $repository->save($account);

            $command = new DepositMoneyCommand($accountId->toString(), $amount);
            $handler = new DepositMoneyCommandHandler($repository);

            // Act
            $exception = null;
            try {
                $handler->handle(command: $command);
            } catch (\InvalidArgumentException $e) {
                $exception = $e;
            }

            // Assert
            expect($exception)->toBeInstanceOf(\InvalidArgumentException::class);
            expect($exception->getMessage())->toBe('Deposit amount must be positive');
        });


        it('throws an exception when account does not exist', function () {
            // Arrange
            $accountId = Uuid::uuid4();
            $amount = 100.00;

            $repository = new InMemoryBankAccountRepository();
            $command = new DepositMoneyCommand($accountId->toString(), $amount);
            $handler = new DepositMoneyCommandHandler($repository);

            // Act
            $exception = null;
            try {
                $handler->handle(command: $command);
            } catch (\Exception $e) {
                $exception = $e;
            }

            // Assert
            expect($exception)->toBeInstanceOf(AccountNotFoundException::class);
            expect($exception->getMessage())->toBe('Account not found');
        });
    });
});
