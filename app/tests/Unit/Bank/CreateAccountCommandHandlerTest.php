<?php

use Application\Bank\Command\CreateAccount\CreateAccountCommand;
use Application\Bank\Command\CreateAccount\CreateAccountCommandHandler;
use Domain\Bank\Events\BankAccountCreated;
use Infrastructure\Symfony\Uuid\FakeUUIDGenerator;

describe('CreateAccountCommandHandler', function () {
    describe('Happy Path', function () {
        it('correctly handles creating a new bank account', function () {
            // Arrange
            $uuidGenerator = new FakeUUIDGenerator();

            $accountType = 'checking';

            $command = new CreateAccountCommand(accountType: $accountType);
            $handler = new CreateAccountCommandHandler(uuidGenerator: $uuidGenerator);

            // Act
            /** @var \Application\Shared\CommandResponse $response */
            $response = $handler->handle(command: $command);
            // Assert
            expect($response->value)->toBe($uuidGenerator->generateId());
            expect($response->events())->toBeArray();
            expect($response->events())->toHaveCount(1);

            /** @var BankAccountCreated */
            $event = current($response->events());
            expect($event)->toBeInstanceOf(BankAccountCreated::class);
            expect($event->id)->toBe($uuidGenerator->generateId());
            expect($event->accountType)->toBe($accountType);
        });
    });

    describe('Unhappy Path', function () {
        it('throws an exception when account type is invalid', function () {
            // Arrange
            $uuidGenerator = new FakeUUIDGenerator();

            $invalidAccountType = '';

            $command = new CreateAccountCommand($invalidAccountType);
            $handler = new CreateAccountCommandHandler($uuidGenerator);

            // Act
            $exception = null;
            try {
                $handler->handle($command);
            } catch (\InvalidArgumentException $e) {
                $exception = $e;
            }

            // Assert
            expect($exception)->toBeInstanceOf(\InvalidArgumentException::class);
            expect($exception->getMessage())->toBe('Account type must be specified');
        });
    });
});
