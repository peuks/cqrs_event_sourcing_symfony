<?php
use Ramsey\Uuid\Uuid;
use Domain\Bank\BankAccount;
use Domain\Bank\Events\MoneyWithdraw;
use Domain\Bank\InMemoryBankAccountRepository;
use Domain\Bank\Exception\AccountNotFoundException;
use Domain\Bank\Exception\InsufficientFundsException;
use Application\Bank\Command\WithdrawMoney\WithdrawMoneyCommand;
use Application\Bank\Command\WithdrawMoney\WithdrawMoneyCommandHandler;

describe('WithdrawMoneyCommandHandler', function () {
    describe('Happy Path', function () {
        it('correctly handles creating a withdraw money command', function () {
            // Arrange
            $accountId = Uuid::uuid4();
            $amount = 50.00;

            // Create a real instance of BankAccount
            $account = new BankAccount($accountId);
            $account->deposit(100.00);
            $repository = new InMemoryBankAccountRepository();
            $repository->save($account);

            $command = new WithdrawMoneyCommand($accountId->toString(), $amount);
            $handler = new WithdrawMoneyCommandHandler($repository);

            // Act
            /** @var \Application\Shared\CommandResponse $response  */
            $response = $handler->handle($command);

            // Assert
            expect($response->value)->toEqual($accountId);
            expect($response->hasEvents())->toBeTrue();
            expect($response->events())->toBeArray();
            expect($response->events())->toHaveCount(2);
        });

        it('correctly emits events after withdraw', function () {
            // Arrange
            $accountId = Uuid::uuid4();
            $amount = 50.00;

            // Create a real instance of BankAccount
            $account = new BankAccount($accountId);
            $account->deposit(100.00);
            $repository = new InMemoryBankAccountRepository();
            $repository->save($account);

            $command = new WithdrawMoneyCommand($accountId->toString(), $amount);
            $handler = new WithdrawMoneyCommandHandler($repository);

            // Act
            /** @var \Application\Shared\CommandResponse $response  */
            $response = $handler->handle($command);

            // Assert
            expect($response->hasEvents())->toBeTrue();
            expect($response->events())->toBeArray();
            expect($response->events()[1])->toBeInstanceOf(MoneyWithdraw::class);
        });
    });

    describe('Unhappy Path', function () {
        it('throws an exception when account does not exist', function () {
            // Arrange
            $accountId = Uuid::uuid4();
            $amount = 50.00;

            $repository = new InMemoryBankAccountRepository();
            $command = new WithdrawMoneyCommand($accountId->toString(), $amount);
            $handler = new WithdrawMoneyCommandHandler($repository);

            // Act
            $exception = null;
            try {
                $handler->handle($command);
            } catch (\Exception $e) {
                $exception = $e;
            }

            // Assert
            expect($exception)->toBeInstanceOf(AccountNotFoundException::class);
            expect($exception->getMessage())->toBe('Account not found');
        });

        it('throws an exception when trying to withdraw more than the balance', function () {
            // Arrange
            $accountId = Uuid::uuid4();
            $amount = 150.00;

            // Create a real instance of BankAccount
            $account = new BankAccount($accountId);
            $account->deposit(100.00);
            $repository = new InMemoryBankAccountRepository();
            $repository->save($account);

            $command = new WithdrawMoneyCommand($accountId->toString(), $amount);
            $handler = new WithdrawMoneyCommandHandler($repository);

            // Act
            $exception = null;
            try {
                $handler->handle($command);
            } catch (\Exception $e) {
                $exception = $e;
            }

            // Assert
            expect($exception)->toBeInstanceOf(InsufficientFundsException::class);
            expect($exception->getMessage())->toBe('Insufficient funds');
        });
    });
});
