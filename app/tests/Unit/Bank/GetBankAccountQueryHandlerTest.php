<?php

use Ramsey\Uuid\Uuid;
use Domain\Bank\BankAccount;
use Domain\Bank\InMemoryBankAccountRepository;
use Application\Bank\Query\GetBankAccount\GetBankAccountQuery;
use Infrastructure\Symfony\Uuid\FakeUUIDGenerator;
use Domain\Bank\Exception\AccountNotFoundException;
use Application\Bank\Query\GetBankAccount\GetBankAccountQueryHandler;
use Application\Bank\Query\GetBankAccount\GetBankAccountQueryResponse;

describe('GetBankAccountQueryHandler', function () {
    describe('Happy Path', function () {
        it('returns the bank account details for an existing account', function () {
            // Arrange
            $repository = new InMemoryBankAccountRepository();
            $accountType = 'Checking';
            // Use the FakeUUIDGenerator
            $accountId = (new FakeUUIDGenerator())->generateId();

            // Create a real instance of BankAccount
            $account = new BankAccount(id: Uuid::fromString(uuid: $accountId));
            $account->create(accountType: $accountType);

            $repository->save(account: $account);

            $query = new GetBankAccountQuery(id: $accountId); // Named parameter
            $handler = new GetBankAccountQueryHandler(repository: $repository);

            // Act
            /** @var GetBankAccountQueryResponse $response */
            $response = $handler->handle(query: $query);

            // Assert
            expect($response->value->snapshot())->toBe(expected: [
                'id' => $accountId,
                'balance' => 0.0,
                'accountType' => $accountType
            ]);
        });
    });

    describe('Unhappy Path', function () {
        it('throws an exception when the account does not exist', function () {
            // Arrange
            $uuidGenerator = new FakeUUIDGenerator();

            // Use the FakeUUIDGenerator
            $accountId = $uuidGenerator->generateId();

            $repository = new InMemoryBankAccountRepository();
            $query = new GetBankAccountQuery(id: $accountId); // Named parameter
            $handler = new GetBankAccountQueryHandler(repository: $repository);

            // Act
            $exception = null;
            try {
                $handler->handle($query);
            } catch (\Exception $e) {
                $exception = $e;
            }

            // Assert
            expect(value: $exception)->toBeInstanceOf(class: AccountNotFoundException::class);
            expect(value: $exception->getMessage())->toBe(expected: 'Account not found');
        });
    });
});
