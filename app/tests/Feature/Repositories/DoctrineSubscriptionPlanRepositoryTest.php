<?php
use Domain\Subscription\Money;
use Infrastructure\Symfony\Repository\SubscriptionPlanRepository;
use Domain\Subscription\SubscriptionPlan as DomainSubscriptionPlan;
use Infrastructure\Symfony\Entity\SubscriptionPlan as EntitySubscriptionPlan;


beforeEach(function () {
    $this->managerRegistry = $this->getContainer()->get('doctrine');
    $this->entityManager = $this->managerRegistry->getManager();

    $this->entityManager->beginTransaction();

    $this->repository = new SubscriptionPlanRepository($this->managerRegistry);
});

afterEach(function () {
    $this->entityManager->rollback();
    $this->entityManager->close();
});

describe('Subscription Plan Repository ', function () {
    describe('Happy Path ', function () {


        it('can list all subscription plans', function () {
            // Arrange: Create and persist multiple plans with detailed properties
            $expectedSnapshots = [];
            for ($i = 0; $i < 3; $i++) {
                $uuid = Ramsey\Uuid\Uuid::uuid4();
                $subscriptionPlan = new EntitySubscriptionPlan();
                $subscriptionPlan->setId($uuid);
                $subscriptionPlan->setName("Plan $i");
                $subscriptionPlan->setPrice(100 + $i * 50);
                $subscriptionPlan->setAnnualDiscount(0.05 * $i);
                $subscriptionPlan->setCurrency('EUR');
                $subscriptionPlan->setCreatedAt(new DateTime());
                $subscriptionPlan->setUpdatedAt(new DateTime());
                $subscriptionPlan->setExpirationDate(new DateTime('+1 year'));
                $subscriptionPlan->setIncludedActivities(['Activity1', 'Activity2']);

                $this->entityManager->persist($subscriptionPlan);
                $this->entityManager->flush();

                $expectedSnapshots[] = [
                    'id' => $uuid->toString(),
                    'name' => "Plan $i",
                    'price' => 100 + $i * 50,
                    'annualDiscount' => 0.05 * $i,
                    'currency' => 'EUR',
                    'createdAt' => $subscriptionPlan->getCreatedAt()->format(DateTime::ATOM),
                    'updatedAt' => $subscriptionPlan->getUpdatedAt()->format(DateTime::ATOM),
                    'expirationDate' => $subscriptionPlan->getExpirationDate()->format(DateTime::ATOM),
                    'includedActivities' => ['Activity1', 'Activity2']
                ];
            }

            // Act: Get all plans
            $plans = $this->repository->getAll();
            // Assert: All plans are retrieved and match expected snapshots
            expect($plans)->toBeArray()->and($plans)->toHaveLength(3);


        });

        it('can retrieve a subscription plan by UUID', function () {
            // Arrange: Define the expected state of the subscription plan
            $uuid = Ramsey\Uuid\Uuid::uuid4();
            $expectedSnapshot = [
                'id' => $uuid,
                'name' => 'Basic Plan',
                'price' => ['amount' => 100.00, 'currency' => 'EUR'],
                'annualDiscount' => 0.1,
                'expirationDate' => (new DateTime('+1 year'))->format(DateTime::ATOM),
                'includedActivities' => ['Yoga', 'Gym'],
                'createdAt' => (new DateTime())->format(DateTime::ATOM),
                'updatedAt' => (new DateTime())->format(DateTime::ATOM)
            ];

            // Create a subscription plan and persist it using the expected snapshot
            $subscriptionPlan = new EntitySubscriptionPlan();
            $subscriptionPlan->setId($expectedSnapshot['id']->toString());
            $subscriptionPlan->setName($expectedSnapshot['name']);
            $subscriptionPlan->setPrice($expectedSnapshot['price']['amount']);
            $subscriptionPlan->setCurrency($expectedSnapshot['price']['currency']);
            $subscriptionPlan->setAnnualDiscount($expectedSnapshot['annualDiscount']);
            $subscriptionPlan->setExpirationDate(new DateTime($expectedSnapshot['expirationDate']));
            $subscriptionPlan->setIncludedActivities($expectedSnapshot['includedActivities']);
            $subscriptionPlan->setCreatedAt(new DateTime($expectedSnapshot['createdAt']));
            $subscriptionPlan->setUpdatedAt(new DateTime($expectedSnapshot['updatedAt']));

            $this->entityManager->persist($subscriptionPlan);
            $this->entityManager->flush();

            // Act: Retrieve the plan by UUID
            $foundPlan = $this->repository->get($uuid);


            // Assert: Use snapshot for comparison
            expect($foundPlan->snapshot())->toEqual($expectedSnapshot);
        });



        it('can add a new subscription plan', function () {
            // Arrange: Define the expected state of the new subscription plan
            $uuid = Ramsey\Uuid\Uuid::uuid4();
            $expectedSnapshot = [
                'id' => $uuid->toString(),
                'name' => 'Premium Plan',
                'price' => ['amount' => 200.00, 'currency' => 'EUR'],
                'annualDiscount' => 0.1,
                'expirationDate' => (new DateTime('+1 year'))->format(DateTime::ATOM),
                'includedActivities' => ['Gym', 'Swimming'],
                'createdAt' => (new DateTime())->format(DateTime::ATOM),
                'updatedAt' => (new DateTime())->format(DateTime::ATOM)
            ];

            // Create a new subscription plan using the expected snapshot
            $domainPlan = new DomainSubscriptionPlan(
                $expectedSnapshot['name'],
                Money::EURO(amount: $expectedSnapshot['price']['amount']),
                $expectedSnapshot['annualDiscount'],
                new DateTime($expectedSnapshot['expirationDate']),
                $expectedSnapshot['includedActivities'],
                $expectedSnapshot['id'],
                new DateTime($expectedSnapshot['createdAt']),
                new DateTime($expectedSnapshot['updatedAt'])
            );

            // Act: Add the new subscription plan to the repository
            $this->repository->add($domainPlan);
            $this->entityManager->flush();

            // Assert: Retrieve and compare the added plan's snapshot to ensure it matches the expected state
            $persistedPlan = $this->repository->get($uuid);
            expect($persistedPlan->snapshot())->toEqual($expectedSnapshot);
        });


        it('can delete a subscription plan', function () {
            // Arrange: Create and persist a plan
            $uuid = Ramsey\Uuid\Uuid::uuid4();
            $subscriptionPlan = new EntitySubscriptionPlan();
            $subscriptionPlan->setId($uuid);
            $subscriptionPlan->setName('Temporary Plan');
            $subscriptionPlan->setPrice(150);
            $subscriptionPlan->setAnnualDiscount(0.05);
            $subscriptionPlan->setCurrency('USD');
            $subscriptionPlan->setCreatedAt(new DateTime());
            $subscriptionPlan->setUpdatedAt(new DateTime());
            $subscriptionPlan->setExpirationDate(new DateTime('+1 year'));
            $subscriptionPlan->setIncludedActivities(['Temporary Access']);

            $this->entityManager->persist($subscriptionPlan);
            $this->entityManager->flush();

            // Act: Delete the plan
            $this->repository->delete($uuid);
            $this->entityManager->flush();

            // Assert: The plan is removed from the database
            $deletedPlan = $this->entityManager->getRepository(EntitySubscriptionPlan::class)->find($uuid);
            expect($deletedPlan)->toBeNull();
        });
    });

});
