<?php

use Ramsey\Uuid\Uuid;
use Domain\Bank\Events\MoneyDeposited;
use Domain\Bank\Events\BankAccountCreated;
use Infrastructure\Symfony\Repository\EventStoreRepository;

describe('Withdraw Money', function () {

    describe('Happy path', function () {
        it('should withdraw money from an account with valid data', function () {
            // Create client and setup test
            $client = static::createClient();
            $container = static::getContainer();
            $eventStoreRepository = $container->get(EventStoreRepository::class);
            $accountId = Uuid::uuid4()->toString();

            // Given an account exist with a minimum amount Create account and store the event
            $event = new BankAccountCreated(
                id: $accountId,
                accountType: 'SAVINGS'
            );

            $event1 = new MoneyDeposited(
                id: $accountId,
                amount: 100.00
            );
            foreach ([$event, $event1] as $event) {
                $eventStoreRepository->store($event);
            }

            // Prepare request data
            $requestData = json_encode([
                'accountId' => $accountId,
                'amount' => 1
            ]);

            // Perform the request
            $client->request('POST', '/bank/withdraw', [], [], ['CONTENT_TYPE' => 'application/json'], $requestData);
            $response = $client->getResponse();

            expect($response->getStatusCode())->toBe(200);

            $responseData = json_decode($response->getContent(), true);
            expect($responseData['data'])->toHaveKey('id');
        });
    });

    describe('Unhappy path', function () {
        it('should return error with insufficient funds', function () {
            // Create client and setup test
            $client = static::createClient();
            $container = static::getContainer();
            $eventStoreRepository = $container->get(EventStoreRepository::class);
            $accountId = Uuid::uuid4()->toString();

            // Create account and store the event
            $event = new BankAccountCreated(
                id: $accountId,
                accountType: 'SAVINGS'
            );

            $eventStoreRepository->store($event);


            // Prepare request data for withdrawal
            $requestData = json_encode([
                'accountId' => $accountId,
                'amount' => 100.00 // More than the deposited amount
            ]);

            // Perform the request
            $client->request(
                'POST',
                '/bank/withdraw',
                [],
                [],
                ['CONTENT_TYPE' => 'application/json'],
                $requestData
            );
            $response = $client->getResponse();
            // Assert the response
            expect($response->getStatusCode())->toBe(500);
            $responseData = json_decode($response->getContent(), true);
            expect($responseData['detail'])->toBe('Insufficient funds');
        });

        it('should return error with invalid account ID', function () {
            // Create client and setup test
            $client = static::createClient();

            // Prepare request data
            $requestData = [
                'accountId' => 'invalid-uuid',
                'amount' => 1
            ];
            // Perform the request
            $client->request(
                'POST',
                '/bank/withdraw',
                [],
                [],
                [
                    'CONTENT_TYPE' => 'application/json',
                    'HTTP_ACCEPT' => 'application/json'
                ],
                json_encode($requestData)
            );

            $response = $client->getResponse();


            expect($response->getStatusCode())->toBe(422);
            $responseData = json_decode($client->getResponse()->getContent(), true);

            expect($responseData['detail'])->toBe("accountId: The account ID must be a valid UUID.");
        });

        it('should return error with missing amount', function () {
            // Create client and setup test
            $client = static::createClient();
            $container = static::getContainer();
            $eventStoreRepository = $container->get(EventStoreRepository::class);
            $accountId = Uuid::uuid4()->toString();

            // Create account and store the event
            $event = new BankAccountCreated(
                id: $accountId,
                accountType: 'SAVINGS'
            );
            $eventStoreRepository->store($event);

            // Prepare request data
            $requestData = [
                'accountId' => $accountId,
            ];

            // Perform the request
            $client->request(
                'POST',
                '/bank/withdraw',
                [],
                [],
                [
                    'CONTENT_TYPE' => 'application/json',
                    'HTTP_ACCEPT' => 'application/json'
                ],
                json_encode($requestData)
            );
            $response = $client->getResponse();

            expect($response->getStatusCode())->toBe(422);

            $responseData = json_decode($response->getContent(), true);
            expect($responseData['detail'])->toBe('amount: This value should be of type float.');
        });

        it('should return error with negative amount', function () {
            // Create client and setup test
            $client = static::createClient();
            $container = static::getContainer();
            $eventStoreRepository = $container->get(EventStoreRepository::class);
            $accountId = Uuid::uuid4()->toString();

            // Create account and store the event
            $event = new BankAccountCreated(
                id: $accountId,
                accountType: 'SAVINGS'
            );
            $eventStoreRepository->store($event);

            // Prepare request data
            $requestData = [
                'accountId' => $accountId,
                'amount' => -1
            ];

            // Perform the request
            $client->request(
                'POST',
                '/bank/withdraw',
                [],
                [],
                [
                    'CONTENT_TYPE' => 'application/json',
                    'HTTP_ACCEPT' => 'application/json'
                ],
                json_encode($requestData)
            );
            $response = $client->getResponse();

            expect($response->getStatusCode())->toBe(422);
            $responseData = json_decode($response->getContent(), true);
            expect($responseData['detail'])->toBe('amount: The amount must be a positive number.');
        });
    });
});
