<?php

describe('Account Creation', function () {

    describe('Happy path', function () {
        test('it should create an account with valid data', function () {
            $client = static::createClient();

            $requestData = json_encode(value: [
                'accountType' => 'CHEAP'
            ]);

            $client->request('POST', '/bank/create', [], [], ['CONTENT_TYPE' => 'application/json'], $requestData);
            $response = $client->getResponse();

            expect($response->getStatusCode())->toBe(200);

            $responseData = json_decode($response->getContent(), true);
            expect($responseData['data'])->toHaveKey('id');
        });
    });

    describe('Unhappy path', function () {
        test('it should return error with missing accountType', function () {
            $client = static::createClient();

            $client->request('POST', '/bank/create', [], [], ['CONTENT_TYPE' => 'application/json'], json_encode([]));

            $response = $client->getResponse();

            expect($response->getStatusCode())->toBe(422);

            $responseData = json_decode($client->getResponse()->getContent(), true);
            expect($responseData['detail'])->toBe('accountType: This value should be of type string.');
        });
    });
});
