<?php

describe('Subscription Plan Creation', function () {

    describe('Happy path', function () {
        test('it should create a subscription plan with valid data', function () {
            $client = static::createClient();

            $requestData = json_encode([
                'price' => 99.99,
                'name' => 'Premium Plan',
                'annualDiscount' => 0.1,
                'expirationDate' => '2023-12-31',
                'includedActivities' => ['Gym', 'Swimming'],
            ]);

            $client->request('POST', '/plan', [], [], ['CONTENT_TYPE' => 'application/json'], $requestData);

            $response = $client->getResponse();

            expect($response->getStatusCode())->toBe(200);
        });
    });

    describe('Unhappy path', function () {
        test('it should return error with invalid price', function () {
            $client = static::createClient();

            $requestData = json_encode([
                'price' => -99.99,
                'name' => 'Premium Plan',
                'annualDiscount' => 0.1,
                'expirationDate' => '2023-12-31',
                'includedActivities' => ['Gym', 'Swimming'],
            ]);

            $client->request('POST', '/plan', [], [], ['CONTENT_TYPE' => 'application/json'], $requestData);

            $response = $client->getResponse();
            expect($response->getStatusCode())->toBe(422);

            $responseData = json_decode($response->getContent(), true);
            expect($responseData['detail'])->toBe('price: The price must be a positive number.');
        });

    });
});
