<?php
declare(strict_types=1);

namespace Application\Shared\Contracts;

/**
 * Abstract base class for command responses that may include multiple events.
 * 
 * @param mixed $value The primary value or result of the command.
 * @param Event[] $events An array of Event objects associated with the command.
 */
abstract class CommandResponse
{
    private array $events = [];

    /**
     * Initializes a new instance of the CommandResponse class with the specified value and events.
     *
     * @param mixed $value The primary value or result of the command.
     * @param Event[] $events An array of Event objects associated with the command.
     */
    protected function __construct(
        public readonly mixed $value,
        array $events
    ) {
        $this->events = $events;
    }

    /**
     * Creates a new command response with a value and associated events.
     *
     * @param mixed $value The primary value of the command.
     * @param Event[] $events An array of events related to the command.
     * @return static Returns a new instance of this class.
     */
    public static function withValue(mixed $value, array $events): self
    {
        return new static(value: $value, events: $events);
    }

    /**
     * Determines whether this command response includes any events.
     *
     * @return bool Returns true if there are events, otherwise false.
     */
    public function hasEvents(): bool
    {
        return !empty($this->events);
    }

    /**
     * Gets the events associated with this command response.
     *
     * @return Event[] Returns an array of Event objects.
     */
    public function events(): array
    {
        return $this->events;
    }
}
