<?php
namespace Application\Shared\Contracts;

interface UUIDGenerator
{
    public function generateId(): string;
}