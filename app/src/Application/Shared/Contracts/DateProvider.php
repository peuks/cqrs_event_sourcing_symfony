<?php
declare(strict_types=1);
namespace Application\Shared\Contracts;

interface DateProvider
{
    public function currentDateTime(): \DateTime;
}
