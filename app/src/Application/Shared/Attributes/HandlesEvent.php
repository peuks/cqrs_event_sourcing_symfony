<?php
declare(strict_types=1);

namespace Application\Shared\Attributes;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS)]
readonly class HandlesEvent
{
    public function __construct(public array $events)
    {
    }
}