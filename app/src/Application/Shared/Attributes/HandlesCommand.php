<?php
declare(strict_types=1);

namespace Application\Shared\Attributes;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS)]
readonly class HandlesCommand
{
    public function __construct(public string $commandType)
    {
    }
}
