<?php

namespace Application\Subscription\Command;

use Domain\Subscription\Money;
use Ramsey\Uuid\Nonstandard\Uuid;
use Application\Shared\Contracts\Command;
use Domain\Subscription\SubscriptionPlan;
use Domain\Subscription\Events\MemberJoined;
use Application\Shared\Contracts\DateProvider;
use Application\Shared\Contracts\UUIDGenerator;
use Application\Shared\Attributes\HandlesCommand;
use Domain\Subscription\SubscriptionPlanRepository;
use Application\Services\Handlers\CommandHandlers\ICommandHandler;

#[HandlesCommand(CreateSubscriptionPlanCommand::class)]
class CreateSubscriptionPlanCommandHandler implements ICommandHandler
{
    public function __construct(
        private SubscriptionPlanRepository $repository,
        private UUIDGenerator $uuidGenerator,
        private DateProvider $dateProvider
    ) {
    }

    /**
     * @param CreateSubscriptionPlanCommand $command
     */
    function handle(Command $command): CommandResponse
    {

        $subscriptionId = $this->uuidGenerator->generateId();

        $subscription = new SubscriptionPlan(
            name: $command->name,
            createdAt: $this->dateProvider->currentDateTime(),
            updatedAt: $this->dateProvider->currentDateTime(),
            price: Money::EURO(amount: $command->price),
            annualDiscount: $command->annualDiscount,
            expirationDate: new \DateTime(datetime: $command->expirationDate),
            includedActivities: $command->includedActivities,
            id: $subscriptionId
        );
        $this->repository->add(subscriptionPlan: $subscription);

        return CommandResponse::withValue(
            value: $subscriptionId,
            events: [
                (
                    new MemberJoined(
                        newMemberId: Uuid::uuid4(),
                        chosenPlanId: Uuid::uuid4()
                    )
                )
            ]
        );
    }

}
