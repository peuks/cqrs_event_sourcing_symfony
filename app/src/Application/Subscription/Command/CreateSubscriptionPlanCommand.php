<?
declare(strict_types=1);
namespace Application\Subscription\Command;

use Application\Shared\Contracts\Command;


readonly class CreateSubscriptionPlanCommand implements Command
{
    public function __construct(
        public float $price,
        public string $name,
        public float $annualDiscount,
        public string $expirationDate,
        public array $includedActivities,
    ) {
    }
}