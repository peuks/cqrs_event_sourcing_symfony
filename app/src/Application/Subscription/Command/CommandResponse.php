<?php
declare(strict_types=1);

namespace Application\Subscription\Command;

/**
 * Command response specific to subscription commands.
 */
class CommandResponse extends \Application\Shared\Contracts\CommandResponse
{
    /**
     * Creates a new command response for subscription commands with a value and associated events.
     *
     * @param mixed $value The primary value of the command.
     * @param \Application\Shared\Contracts\Event[] $events An array of events related to the command.
     * @return self Returns a new instance of this class.
     */
    public static function withValue(mixed $value, array $events): self
    {
        return new self(value: $value, events: $events);
    }
}
