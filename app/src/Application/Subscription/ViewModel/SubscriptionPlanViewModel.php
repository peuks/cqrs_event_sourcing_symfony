<?php
declare(strict_types=1);
namespace Application\Subscription\ViewModel;

use Application\Shared\Contracts\ViewModel;
use Domain\Subscription\SubscriptionPlan;


readonly class SubscriptionPlanViewModel implements ViewModel
{
    public function __construct(
        public string $id,
        public string $name,
        public float $price,
        public float $annualDiscount,
        public string $expirationDate,
        public array $includedActivities
    ) {

    }

    public static function fromEntity(SubscriptionPlan $plan): self
    {
        $snapshot = $plan->snapshot();

        return new self(
            id: (string) $snapshot['id'],
            name: $snapshot['name'],
            price: $snapshot['price']['amount'],
            annualDiscount: $snapshot['annualDiscount'],
            expirationDate: $snapshot['expirationDate'],
            includedActivities: $snapshot['includedActivities']
        );
    }
}