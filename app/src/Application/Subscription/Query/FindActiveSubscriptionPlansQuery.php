<?php
declare(strict_types=1);
namespace Application\Subscription\Query;

use Application\Shared\Contracts\Query;
use Ramsey\Uuid\UuidInterface;

class FindActiveSubscriptionPlansQuery implements Query
{
    public function __construct(public readonly UuidInterface $id)
    {
    }

}