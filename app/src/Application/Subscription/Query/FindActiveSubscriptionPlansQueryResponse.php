<?php
declare(strict_types=1);
namespace Application\Subscription\Query;

use Application\Shared\Contracts\QueryResponse;

class FindActiveSubscriptionPlansQueryResponse extends QueryResponse
{

}