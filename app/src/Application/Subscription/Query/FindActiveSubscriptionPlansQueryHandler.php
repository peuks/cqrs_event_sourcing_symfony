<?php
declare(strict_types=1);
namespace Application\Subscription\Query;

use Ramsey\Uuid\Nonstandard\Uuid;
use Application\Shared\Contracts\Query;
use Domain\Subscription\SubscriptionPlan;
use Application\Shared\Attributes\HandlesQuery;
use Application\Shared\Contracts\QueryResponse;
use Domain\Subscription\SubscriptionPlanRepository;
use Application\Services\Handlers\QueryHandlers\IQueryHandler;
use Application\Subscription\ViewModel\SubscriptionPlanViewModel;


#[HandlesQuery(FindActiveSubscriptionPlansQuery::class)]
class FindActiveSubscriptionPlansQueryHandler implements IQueryHandler
{
    public function __construct(private SubscriptionPlanRepository $repository)
    {

    }
    /** @param  FindActiveSubscriptionPlansQuery $query   */
    function handle(
        Query $query
    ): QueryResponse {
        return new FindActiveSubscriptionPlansQueryResponse(
            value:
            array_map(
                fn(SubscriptionPlan $plan) =>
                SubscriptionPlanViewModel::fromEntity(plan: $plan),
                $this->repository->findActivePlan(id: $query->id)
            ),
            events: []
        );
    }
}
