<?php
namespace Application\Bank\Query\GetBankAccount;

use Ramsey\Uuid\Uuid;
use Domain\Bank\BankAccountRepository;
use Application\Shared\Contracts\Query;
use Application\Shared\Attributes\HandlesQuery;
use Application\Shared\Contracts\QueryResponse;
use Domain\Bank\Exception\AccountNotFoundException;
use Application\Bank\Query\GetBankAccount\GetBankAccountQuery;
use Application\Services\Handlers\QueryHandlers\IQueryHandler;
use Application\Bank\Query\GetBankAccount\GetBankAccountQueryResponse;

#[HandlesQuery(GetBankAccountQuery::class)]
class GetBankAccountQueryHandler implements IQueryHandler
{
    public function __construct(private BankAccountRepository $repository)
    {
    }

    /** @param GetBankAccountQuery $query */
    public function handle(Query $query): QueryResponse
    {
        $bankaccount = $this->repository->get(id: Uuid::fromString(uuid: $query->id));

        if (!$bankaccount)
            throw new AccountNotFoundException();

        return new GetBankAccountQueryResponse(value: $bankaccount, events: []);
    }

}

