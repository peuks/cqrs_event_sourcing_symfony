<?php
declare(strict_types=1);
namespace Application\Bank\Query\GetBankAccount;

use Application\Shared\Contracts\QueryResponse;

class GetBankAccountQueryResponse extends QueryResponse
{
}