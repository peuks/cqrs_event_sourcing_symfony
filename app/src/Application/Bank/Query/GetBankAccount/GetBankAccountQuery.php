<?php
namespace Application\Bank\Query\GetBankAccount;

use Application\Shared\Contracts\Query;

readonly class GetBankAccountQuery implements Query
{
    public function __construct(
        public string $id
    ) {
    }
}