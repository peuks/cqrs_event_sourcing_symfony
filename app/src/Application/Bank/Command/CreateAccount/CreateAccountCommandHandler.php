<?php
namespace Application\Bank\Command\CreateAccount;

use Ramsey\Uuid\Uuid;
use Domain\Bank\BankAccount;
use Application\Shared\CommandResponse;
use Application\Shared\Contracts\Command;
use Application\Shared\Contracts\UUIDGenerator;
use Application\Shared\Attributes\HandlesCommand;
use Application\Services\Handlers\CommandHandlers\ICommandHandler;


#[HandlesCommand(CreateAccountCommand::class)]
class CreateAccountCommandHandler implements ICommandHandler
{

    public function __construct(private UUIDGenerator $uuidGenerator)
    {
    }

    /** @param CreateAccountCommand $command */
    public function handle(Command $command): CommandResponse
    {

        $bankAccount = new BankAccount(id: Uuid::fromString(uuid: $this->uuidGenerator->generateId()));

        $events = $bankAccount->create(accountType: $command->accountType);

        return CommandResponse::withValue(
            value: $bankAccount->id->toString(),
            events: $events
        );
    }
}