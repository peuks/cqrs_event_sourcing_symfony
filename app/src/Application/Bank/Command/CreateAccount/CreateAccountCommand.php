<?php
declare(strict_types=1);

namespace Application\Bank\Command\CreateAccount;

use Application\Shared\Contracts\Command;

readonly class CreateAccountCommand implements Command
{
    public function __construct(public string $accountType)
    {
    }
}