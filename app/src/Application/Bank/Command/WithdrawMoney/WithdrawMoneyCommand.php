<?php
declare(strict_types=1);

namespace Application\Bank\Command\WithdrawMoney;

use Application\Shared\Contracts\Command;
use Application\Shared\Attributes\StorableEvent;

#[StorableEvent]
readonly class WithdrawMoneyCommand implements Command
{


    public function __construct(public string $accountId, public float $amount)
    {

    }
}
