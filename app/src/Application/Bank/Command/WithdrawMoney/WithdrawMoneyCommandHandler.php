<?php
declare(strict_types=1);

namespace Application\Bank\Command\WithdrawMoney;

use Ramsey\Uuid\Uuid;
use Domain\Bank\BankAccountRepository;
use Application\Shared\CommandResponse;
use Application\Shared\Contracts\Command;
use Application\Shared\Attributes\HandlesCommand;
use Domain\Bank\Exception\AccountNotFoundException;
use Application\Services\Handlers\CommandHandlers\ICommandHandler;

#[HandlesCommand(WithdrawMoneyCommand::class)]
class WithdrawMoneyCommandHandler implements ICommandHandler
{


    public function __construct(private BankAccountRepository $repository)
    {

    }

    /**
     * @param WithdrawMoneyCommand $command
     */
    public function handle(Command $command): CommandResponse
    {
        $account = $this->repository->get(Uuid::fromString($command->accountId));

        if ($account === null)
            throw new AccountNotFoundException();

        $events = $account->withdraw(amount: $command->amount);

        return CommandResponse::withValue(
            value: $account->id,
            events: $events
        );
    }
}
