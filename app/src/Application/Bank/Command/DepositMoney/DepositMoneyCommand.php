<?php
declare(strict_types=1);

namespace Application\Bank\Command\DepositMoney;

use Application\Shared\Contracts\Command;

readonly class DepositMoneyCommand implements Command
{
    public function __construct(public string $accountId, public float $amount)
    {
    }
}
