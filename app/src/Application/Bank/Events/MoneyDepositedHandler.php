<?php

declare(strict_types=1);

namespace Application\Bank\Events;

use Domain\Bank\Events\MoneyDeposited;
use Application\Shared\Contracts\Event;
use Application\Shared\Attributes\HandlesEvent;
use Domain\Bank\BankAccountPerMonthProjectionRepository;
use Domain\Bank\Projections\BankAccountMonthlyProjection;
use Application\Services\Handlers\EventHandlers\IEventHandler;

#[HandlesEvent(events: [MoneyDeposited::class])]
class MoneyDepositedHandler implements IEventHandler
{
    public function __construct(private BankAccountPerMonthProjectionRepository $repository)
    {
    }

    /** @param MoneyDeposited $event */
    public function handle(Event $event): void
    {
        $month = $this->getCurrentMonth();
        $projection = $this->repository->get($event->id, $month);
        if ($projection === null)
            return;

        $this->repository->update(
            projection: BankAccountMonthlyProjection::create(
                accountId: $projection->accountId,
                month: $projection->month,
                balance: $projection->balance + $event->amount,
                totalDeposits: $projection->totalDeposits + $event->amount,
                totalWithdrawals: $projection->totalWithdrawals
            )
        );
        return;
    }

    private function getCurrentMonth(): string
    {
        return (new \DateTime())->format('Y-m');
    }
}
