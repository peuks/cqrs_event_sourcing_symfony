<?php
declare(strict_types=1);
namespace Application\Bank\Events;

use Application\Shared\Contracts\Event;
use Domain\Bank\Events\BankAccountCreated;
use Application\Shared\Contracts\DateProvider;
use Application\Shared\Attributes\HandlesEvent;
use Domain\Bank\BankAccountPerMonthProjectionRepository;
use Domain\Bank\Projections\BankAccountMonthlyProjection;
use Application\Services\Handlers\EventHandlers\IEventHandler;


#[HandlesEvent(events: [BankAccountCreated::class])]
class BankAccountCreatedHandler implements IEventHandler
{
    public function __construct(
        private BankAccountPerMonthProjectionRepository $repository,
        private DateProvider $dateProvider
    ) {
    }

    /** @param BankAccountCreated $event */
    public function handle(Event $event): void
    {
        $month = $this->getCurrentMonth();

        $existingProjection = $this->repository->get(accountId: $event->id, month: $month);

        if ($existingProjection !== null)
            return;

        $this->repository->save(
            projection: BankAccountMonthlyProjection::create(
                accountId: $event->id,
                month: $month,
                createdAt: $this->dateProvider->currentDateTime(),
                updatedAt: $this->dateProvider->currentDateTime()
            )
        );
    }

    private function getCurrentMonth(): string
    {
        return (new \DateTime())->format('Y-m');
    }
}
