<?php
namespace Application\Services\EventBus;

use Application\Shared\Contracts\Event;

interface IEventBus
{
    public function dispatch(Event $command): void;
}
