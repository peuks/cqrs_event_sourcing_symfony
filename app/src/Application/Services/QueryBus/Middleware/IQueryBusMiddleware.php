<?php
declare(strict_types=1);
namespace Application\Services\QueryBus\Middleware;

use Application\Shared\Contracts\Query;
use Application\Shared\Contracts\QueryResponse;


interface IQueryBusMiddleware
{
    public function dispatch(Query $query): QueryResponse;
}
