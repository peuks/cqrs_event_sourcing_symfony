<?php
namespace Application\Services\QueryBus;

use Application\Shared\Contracts\Query;
use Application\Shared\Contracts\QueryResponse;

interface IQueryBus
{
    public function dispatch(Query $query): QueryResponse;
}
