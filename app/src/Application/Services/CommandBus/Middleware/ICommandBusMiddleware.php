<?php
declare(strict_types=1);
namespace Application\Services\CommandBus\Middleware;

use Application\Shared\Contracts\Command;
use Application\Shared\Contracts\CommandResponse;


interface ICommandBusMiddleware
{
    public function dispatch(Command $command): CommandResponse;
}