<?php
namespace Application\Services\CommandBus;

use Application\Shared\Contracts\Command;
use Application\Shared\Contracts\CommandResponse;

interface ICommandBus
{
    public function dispatch(Command $command): CommandResponse;
}
