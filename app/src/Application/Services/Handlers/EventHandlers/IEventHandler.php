<?
declare(strict_types=1);
namespace Application\Services\Handlers\EventHandlers;

use Application\Shared\Contracts\Event;

interface IEventHandler
{
    public function handle(Event $event): void;

}