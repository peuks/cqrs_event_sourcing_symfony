<?
declare(strict_types=1);

namespace Application\Services\Handlers\CommandHandlers;

use Application\Shared\Contracts\Command;
use Application\Shared\Contracts\CommandResponse;

interface ICommandHandler
{
    public function handle(Command $command): CommandResponse;
}