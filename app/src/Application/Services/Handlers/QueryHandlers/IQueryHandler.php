<?
declare(strict_types=1);
namespace Application\Services\Handlers\QueryHandlers;

use Application\Shared\Contracts\Query;
use Application\Shared\Contracts\QueryResponse;

interface IQueryHandler
{
    public function handle(Query $query): QueryResponse;
}