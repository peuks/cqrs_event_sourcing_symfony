<?php
declare(strict_types=1);
namespace Infrastructure\Symfony\Plan;

use Domain\Subscription\Money;
use Domain\Subscription\SubscriptionPlan;
use Domain\Subscription\SubscriptionPlanRepository;
use Ramsey\Uuid\Nonstandard\Uuid;
use Ramsey\Uuid\UuidInterface;

class FakePlanRepository implements SubscriptionPlanRepository
{
    public function get(UuidInterface $id): SubscriptionPlan
    {
        return new SubscriptionPlan(
            name: 'plan',
            price: Money::EURO(123),
            annualDiscount: 0.05,
            expirationDate: new \DateTime(),
            includedActivities: ['tennis table'],
            id: Uuid::fromString('an_given_id'),
        );
    }
    public function add(SubscriptionPlan $subscriptionPlan): void
    {

    }
    public function delete(UuidInterface $id): void
    {

    }

    /**
     * @return \Domain\Subscription\SubscriptionPlan[]
     */
    public function getAll(): array
    {
        return [];
    }

    /**
     * @return SubscriptionPlan[]
     */
    public function findActivePlan(UuidInterface $id): array
    {
        return [
            new SubscriptionPlan(
                name: 'plan',
                price: Money::EURO(123),
                annualDiscount: 0.05,
                expirationDate: new \DateTime(),
                includedActivities: ['tennis table'],
                id: $id->toString(),
            )
        ];
    }
}