<?php
declare(strict_types=1);
namespace Infrastructure\Symfony\Plan;

use Domain\Subscription\Member;
use Ramsey\Uuid\Rfc4122\UuidV4;
use Domain\Subscription\MemberId;
use Domain\Subscription\MemberRepository;

class FakeMemberRepository implements MemberRepository
{
    public function get(MemberId $memberId): Member
    {
        return new Member(
            new MemberId(UuidV4::uuid4()->toString()),
            'David',
            'david@example.com',
            new \DateTime()
        );
    }
}
