<?php
declare(strict_types=1);

namespace Infrastructure\Symfony\Plan;

use InvalidArgumentException;
use Ramsey\Uuid\UuidInterface;
use Domain\Subscription\SubscriptionPlan;
use Domain\Subscription\SubscriptionPlanRepository;

class InMemorySubscriptionPlanRepository implements SubscriptionPlanRepository
{
    private array $plans = [];

    public function get(UuidInterface $id): SubscriptionPlan
    {
        $idString = $id->toString();
        if (!isset($this->plans[$idString])) {
            throw new InvalidArgumentException("Subscription plan with ID {$idString} not found.");
        }
        return $this->plans[$idString];
    }

    public function add(SubscriptionPlan $subscriptionPlan): void
    {
        $snapshot = $subscriptionPlan->snapshot();
        if ($snapshot['id'] === null)
            throw new InvalidArgumentException("Subscription plan must have an ID.");
        $this->plans[$snapshot['id']] = $subscriptionPlan;
    }

    public function delete(UuidInterface $id): void
    {
        $idString = $id->toString();
        if (isset($this->plans[$idString])) {
            unset($this->plans[$idString]);
        }
    }

    public function getAll(): array
    {
        return array_values($this->plans);
    }

    public function findActivePlan(UuidInterface $id): array
    {
        $idString = $id->toString();

        return (isset($this->plans[$idString])) ?
            [$this->plans[$idString]] : [];
    }
}
