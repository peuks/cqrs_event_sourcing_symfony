<?php
declare(strict_types=1);

namespace Infrastructure\Symfony\Uuid;

use Application\Shared\Contracts\UUIDGenerator;

class FakeUUIDGenerator implements UUIDGenerator
{
    public function generateId(): string
    {
        return '00000000-0000-0000-0000-000000000000';
    }
}
