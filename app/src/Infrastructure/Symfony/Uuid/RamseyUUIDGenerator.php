<?php
declare(strict_types=1);
namespace Infrastructure\Symfony\Uuid;

use Ramsey\Uuid\Uuid;
use Application\Shared\Contracts\UUIDGenerator;


class RamseyUUIDGenerator implements UUIDGenerator
{
    public function generateId(): string
    {
        return Uuid::uuid4()->toString();
    }
}