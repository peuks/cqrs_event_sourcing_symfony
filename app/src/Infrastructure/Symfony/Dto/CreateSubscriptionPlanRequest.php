<?php
namespace Infrastructure\Symfony\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class CreateSubscriptionPlanRequest
{
    #[Assert\NotBlank(message: "The price cannot be blank.")]
    #[Assert\Positive(message: "The price must be a positive number.")]
    public float $price;

    #[Assert\NotBlank(message: "The name cannot be blank.")]
    public string $name;

    #[Assert\Range(min: 0, max: 1, notInRangeMessage: "The annual discount must be between 0 and 1.")]
    public float $annualDiscount;

    #[Assert\Date(message: "The expiration date must be a valid date.")]
    #[Assert\NotBlank(message: "The expiration date cannot be blank.")]
    public string $expirationDate;

    #[Assert\All([
        new Assert\Type(type: 'string', message: "All included activities must be strings."),
        new Assert\NotBlank(message: "Included activities cannot be blank."),
    ])]
    public array $includedActivities;
}
