<?php

namespace Infrastructure\Symfony\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class CreateAccountRequest
{
    #[Assert\NotBlank(message: 'Account type cannot be blank.')]
    #[Assert\Type(type: 'string', message: 'Account type must be a string.')]
    public string $accountType;

    public function __construct(string $accountType)
    {
        $this->accountType = $accountType;
    }
}
