<?php

namespace Infrastructure\Symfony\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class WithdrawMoneyRequest
{
    #[Assert\NotBlank(message: "The account ID cannot be blank.")]
    #[Assert\Uuid(message: "The account ID must be a valid UUID.")]
    public string $accountId;

    #[Assert\NotBlank(message: "The amount cannot be blank.")]
    #[Assert\Type(type: 'numeric', message: "The amount must be a numeric value.")]
    #[Assert\Positive(message: "The amount must be a positive number.")]
    public float $amount;

    public function __construct(string $accountId, float $amount)
    {
        $this->accountId = $accountId;
        $this->amount = $amount;
    }
}
