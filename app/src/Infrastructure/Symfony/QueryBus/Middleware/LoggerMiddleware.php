<?php

namespace Infrastructure\Symfony\QueryBus\Middleware;

use Psr\Log\LoggerInterface;
use Application\Shared\Contracts\Query;
use Application\Shared\Contracts\QueryResponse;
use Application\Services\QueryBus\Middleware\IQueryBusMiddleware;



/**
 * Middleware that logs the execution time of command dispatching within a command bus.
 * 
 * It measures the time before and afterApplication\Shared\Query dispatching the command to the next middleware
 * and logs this duration along with the command class name. This can be useful for 
 * monitoring command execution and identifying performance bottlenecks.
 *
 * @package Application\Shared\QueryBus\Middleware
 */
class LoggerMiddleware implements IQueryBusMiddleware
{
    public function __construct(
        private IQueryBusMiddleware $next,
        private LoggerInterface $logger
    ) {
    }

    public function dispatch(Query $query): QueryResponse
    {
        $startTime = microtime(true);

        $response = $this->next->dispatch(query: $query);

        $endTime = microtime(true);

        $elapsed = $endTime - $startTime;

        $this->logger->info('Query dispatched.', [
            'command' => $query::class,
            'duration' => $elapsed,
            'unit' => 'seconds'
        ]);

        return $response;
    }
}
