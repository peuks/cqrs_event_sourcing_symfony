<?php
declare(strict_types=1);

namespace Infrastructure\Symfony\QueryBus\Middleware;

use LogicException;
use ReflectionClass;
use Application\Shared\Contracts\Query;
use Application\Shared\Attributes\HandlesQuery;
use Application\Shared\Contracts\QueryResponse;
use Application\Services\Handlers\QueryHandlers\IQueryHandler;
use Application\Services\QueryBus\Middleware\IQueryBusMiddleware;


/**
 * Dispatches query objects to their corresponding handlers.
 *
 * This dispatcher is responsible for mapping query objects to their respective handlers
 * and then invoking the handler. If a query does not have an associated handler, a
 * LogicException is thrown.
 */
class QueryBusDispatcher implements IQueryBusMiddleware
{
    private array $handlers = [];

    public function __construct(iterable $handlers)
    {
        foreach ($handlers as $handler)
            $this->registerHandler($handler);
    }

    private function registerHandler($handler): void
    {
        $refClass = new ReflectionClass($handler);
        $attributes = $refClass->getAttributes(HandlesQuery::class);


        foreach ($attributes as $attribute) {
            $queryType = $attribute->newInstance()->queryType;
            if (isset($this->handlers[$queryType])) {
                $existingHandlerClass = $this->handlers[$queryType]::class;
                $newHandlerClass = $refClass->getName();
                throw new LogicException("Duplicate query handler registered for query type {$queryType}: Existing handler is {$existingHandlerClass}, attempting to register {$newHandlerClass}.");
            }
            $this->handlers[$queryType] = $handler;
        }
    }
    /**
     * Dispatches a query to the appropriate handler.
     *
     * @param Query $query The query to dispatch.
     * @return QueryResponse The response from the query handler.
     * @throws LogicException If no handler is found for the given query.
     */
    public function dispatch(Query $query): QueryResponse
    {
        $queryClass = $query::class;
        /**
         * @var IQueryHandler $handler   
         */
        $handler = $this->handlers[$queryClass] ?? $this->handleMissingHandler($queryClass);
        return $handler->handle(query: $query);
    }

    /**
     * Handles the case when no query handler is found for the given query class.
     *
     * @param string $queryClass The class name of the query that lacks a handler.
     * @throws LogicException Always thrown in the current implementation to indicate an error.
     */
    private function handleMissingHandler(string $queryClass): void
    {
        throw new LogicException("Handler for query {$queryClass} not found.");
    }
}
