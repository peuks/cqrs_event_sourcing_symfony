<?php
declare(strict_types=1);
namespace Infrastructure\Symfony\QueryBus\Middleware;

use Application\Shared\Contracts\Query;
use Application\Services\EventBus\IEventBus;
use Application\Shared\Contracts\QueryResponse;
use Application\Services\QueryBus\Middleware\IQueryBusMiddleware;

/**
 * EventDispatcherBusMiddleware is responsible for dispatching events that are 
 * raised during the handling of a query. It ensures that after a query is handled, 
 * any resulting events are passed to the event bus for further processing.
 */
class EventDispatcherBusMiddleware implements IQueryBusMiddleware
{
    public function __construct(private IQueryBusMiddleware $next, private IEventBus $eventBus)
    {
    }

    public function dispatch(Query $query): QueryResponse
    {
        $queryResponse = $this->next->dispatch($query);

        if ($queryResponse->hasEvents())
            foreach ($queryResponse->events() as $event)
                $this->eventBus->dispatch($event);

        return $queryResponse;
    }
}
