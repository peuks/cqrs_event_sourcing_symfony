<?php
namespace Infrastructure\Symfony\QueryBus;

use Application\Shared\Contracts\Query;
use Application\Services\QueryBus\IQueryBus;
use Application\Shared\Contracts\QueryResponse;
use Application\Services\QueryBus\Middleware\IQueryBusMiddleware;

class QueryBus implements IQueryBus
{

    public function __construct(private IQueryBusMiddleware $middleware)
    {
    }

    public function dispatch(Query $query): QueryResponse
    {
        return $this->middleware->dispatch($query);
    }
}
