<?php
declare(strict_types=1);
namespace Infrastructure\Symfony\QueryBus;

use Psr\Log\LoggerInterface;
use Application\Services\EventBus\IEventBus;
use Application\Services\QueryBus\IQueryBus;
use Infrastructure\Symfony\QueryBus\Middleware\LoggerMiddleware;
use Infrastructure\Symfony\QueryBus\Middleware\QueryBusDispatcher;
use Infrastructure\Symfony\QueryBus\Middleware\EventDispatcherBusMiddleware;



/**
 * Factory for creating a Query Bus.
 *
 * This class sets up the Query Bus with necessary middleware, particularly
 * with logging capabilities. It ensures that each query processed by the bus
 * is logged for auditing and debugging purposes. The Query Bus is composed of
 * a dispatcher that is responsible for routing queries to their respective handlers.
 * LoggerMiddleware is used to wrap the dispatcher for logging each query's handling.
 */
class QueryBusFactory
{
    public static function build(iterable $handlers, LoggerInterface $logger, IEventBus $eventBus): IQueryBus
    {
        return new QueryBus(
            middleware: new LoggerMiddleware(
                logger: $logger,
                next: new EventDispatcherBusMiddleware(
                    next: new QueryBusDispatcher(handlers: $handlers),
                    eventBus: $eventBus
                )
            )
        );
    }
}