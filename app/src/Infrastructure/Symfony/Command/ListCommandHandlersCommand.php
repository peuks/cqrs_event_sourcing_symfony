<?php

namespace Infrastructure\Symfony\Command;

use Application\Shared\Attributes\HandlesCommand;
use Application\Shared\Attributes\HandlesEvent;
use ReflectionClass;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:list-handlers',
    description: 'Lists all commands and events along with their respective handlers',
)]
class ListCommandHandlersCommand extends Command
{

    public function __construct(
        private iterable $commandHandlers,
        private iterable $eventHandlers
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        // Discover and display command handlers
        $commandRows = $this->discoverCommandHandlers();
        if (!empty($commandRows)) {
            $io->section('Command Handlers');
            $io->table(['Command', 'Handler'], $commandRows);
        } else {
            $io->warning('No command handlers found.');
        }

        // Discover and display event handlers
        $eventRows = $this->discoverEventHandlers();
        if (!empty($eventRows)) {
            $io->section('Event Handlers');
            $io->table(['Event', 'Handler'], $eventRows);
        } else {
            $io->warning('No event handlers found.');
        }

        return Command::SUCCESS;
    }

    /**
     * Discover command-handler mappings.
     *
     * @return array
     */
    private function discoverCommandHandlers(): array
    {
        $mappings = [];

        foreach ($this->commandHandlers as $handler) {
            $refClass = new ReflectionClass($handler);
            $attributes = $refClass->getAttributes(HandlesCommand::class);

            foreach ($attributes as $attribute) {
                $commandType = $attribute->newInstance()->commandType;
                $mappings[] = [$commandType, $refClass->getName()];
            }
        }

        return $mappings;
    }

    /**
     * Discover event-handler mappings.
     *
     * @return array
     */
    private function discoverEventHandlers(): array
    {
        $mappings = [];
        foreach ($this->eventHandlers as $handler) {
            $refClass = new ReflectionClass($handler);
            $attributes = $refClass->getAttributes(HandlesEvent::class);

            foreach ($attributes as $attribute) {
                $events = $attribute->newInstance()->events;
                foreach ($events as $event) {
                    $mappings[] = [$event, $refClass->getName()];
                }
            }
        }

        return $mappings;
    }
}
