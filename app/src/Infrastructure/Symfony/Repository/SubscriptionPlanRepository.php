<?php

namespace Infrastructure\Symfony\Repository;

use Domain\Subscription\Money;
use Ramsey\Uuid\UuidInterface;
use Doctrine\Persistence\ManagerRegistry;
use Domain\Subscription\SubscriptionPlan as DomainSubscriptionPlan;
use Infrastructure\Symfony\Entity\SubscriptionPlan as EntitySubscriptionPlan;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @extends ServiceEntityRepository<EntitySubscriptionPlan>
 */
class SubscriptionPlanRepository extends ServiceEntityRepository implements \Domain\Subscription\SubscriptionPlanRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EntitySubscriptionPlan::class);
    }

    public function get(UuidInterface $id): ?DomainSubscriptionPlan
    {
        $entity = $this->find($id->toString());

        return $entity ? $this->mapToDomainModel(entity: $entity) : null;
    }

    public function add(DomainSubscriptionPlan $subscriptionPlan): void
    {
        $entity = $this->mapFromDomainModel(domainModel: $subscriptionPlan);
        $this->getEntityManager()->persist(object: $entity);
        $this->getEntityManager()->flush();
    }

    public function delete(UuidInterface $id): void
    {
        $entity = $this->find(id: $id->toString());

        if (!$entity)
            return;

        $this->getEntityManager()->remove(object: $entity);
        $this->getEntityManager()->flush();

    }

    public function getAll(): array
    {
        $entities = $this->findAll();


        return empty($entities) ? [] :
            array_map(
                callback: fn(EntitySubscriptionPlan $entitySubscriptionPlan) =>
                $this->mapToDomainModel(entity: $entitySubscriptionPlan),
                array: $entities
            );
    }

    public function findActivePlan(UuidInterface $id): array
    {
        $entities = $this->findBy(criteria:
            [
                'id' => $id->toString(),
                'deletedAt' => null
            ]);


        return empty($entities) ? [] : array_map(
            callback: fn(EntitySubscriptionPlan $entitySubscriptionPlan) =>
            $this->mapToDomainModel(entity: $entitySubscriptionPlan),
            array: $entities
        );
    }

    private function mapToDomainModel(EntitySubscriptionPlan $entity): DomainSubscriptionPlan
    {
        return new DomainSubscriptionPlan(
            name: $entity->getName(),
            id: $entity->getId(),
            createdAt: $entity->getCreatedAt(),
            updatedAt: $entity->getUpdatedAt(),
            price: Money::EURO($entity->getPrice()),
            annualDiscount: $entity->getAnnualDiscount(),
            expirationDate: $entity->getExpirationDate(),
            includedActivities: $entity->getIncludedActivities()
        );
    }

    private function mapFromDomainModel(DomainSubscriptionPlan $domainModel): EntitySubscriptionPlan
    {
        $snapshot = $domainModel->snapshot();
        $entity = $this->find(
            id: $snapshot['id']
        ) ?? new EntitySubscriptionPlan();

        $entity->setName($snapshot['name'])
            ->setId($snapshot['id'])
            ->setPrice($snapshot['price']['amount'])
            ->setCurrency($snapshot['price']['currency'])
            ->setAnnualDiscount($snapshot['annualDiscount'])
            ->setExpirationDate(new \DateTime($snapshot['expirationDate']))
            ->setIncludedActivities($snapshot['includedActivities'])
            ->setCreatedAt(new \DateTime($snapshot['createdAt']))
            ->setUpdatedAt(new \DateTime($snapshot['updatedAt']));

        return $entity;
    }
}
