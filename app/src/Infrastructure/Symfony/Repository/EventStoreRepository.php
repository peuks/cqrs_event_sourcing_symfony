<?php

namespace Infrastructure\Symfony\Repository;

use Application\Shared\Contracts\Event;
use Doctrine\Persistence\ManagerRegistry;
use Infrastructure\Symfony\Entity\EventStore;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @extends ServiceEntityRepository<EventStore>
 *
 * @method EventStore|null find($id, $lockMode = null, $lockVersion = null)
 * @method EventStore|null findOneBy(array $criteria, array $orderBy = null)
 * @method EventStore[]    findAll()
 * @method EventStore[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventStoreRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EventStore::class);
    }

    /**
     * Stores a domain event in the event store.
     *
     * @param Event $event The domain event to store.
     */
    public function store(Event $event): void
    {
        $eventStoreEntity = new EventStore();
        $eventStoreEntity->setAggregateType($event::class);
        $eventStoreEntity->setAggregateId($event?->id);
        $eventStoreEntity->setPayload(serialize($event));
        $eventStoreEntity->setTimestamp(new \DateTimeImmutable());

        $entityManager = $this->getEntityManager();
        $entityManager->persist($eventStoreEntity);
        $entityManager->flush();
    }

    /**
     * Retrieves all events for a specified aggregate type.
     *
     * @param string $aggregateType
     * @return EventStore[]
     */
    public function findByAggregateType(string $aggregateType): array
    {
        return $this->findBy(['aggregateType' => $aggregateType]);
    }

    /**
     * Retrieves all events for a specified aggregate ID.
     *
     * @param string $aggregateId
     * @return EventStore[]
     */
    public function findByAggregateId(string $aggregateId): array
    {
        return $this->findBy(['aggregateId' => $aggregateId]);
    }
}
