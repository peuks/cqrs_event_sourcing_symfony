<?php

namespace Infrastructure\Symfony\Repository;

use Doctrine\DBAL\Connection;
use Domain\Bank\BankAccountPerMonthProjectionRepository;
use Domain\Bank\Projections\BankAccountMonthlyProjection;
use Doctrine\ORM\EntityManagerInterface;
use Infrastructure\Symfony\Entity\BankAccountMonthlyProjection as DoctrineBankAccountMonthlyProjection;

class DoctrineBankAccountPerMonthProjectionAdapter implements BankAccountPerMonthProjectionRepository
{
    public function __construct(
        private Connection $connection,
        private EntityManagerInterface $entityManager,
        private BankAccountMonthlyProjectionRepository $doctrineRepository
    ) {
    }

    /**
     * Sauvegarde la projection dans la base de données.
     *
     * @param BankAccountMonthlyProjection $projection
     */
    public function save(BankAccountMonthlyProjection $projection): void
    {
        $doctrineProjection = $this->mapToDoctrineEntity($projection);
        $this->entityManager->persist($doctrineProjection);
        $this->entityManager->flush();
    }

    /**
     * Récupère la projection pour un compte donné et un mois donné.
     *
     * @param string $accountId
     * @param string $month
     * @return BankAccountMonthlyProjection|null
     */
    public function get(string $accountId, string $month): ?BankAccountMonthlyProjection
    {
        $doctrineProjection = $this->doctrineRepository->findOneBy(criteria: [
            'accountId' => $accountId,
            'month' => $month,
        ]);

        return $doctrineProjection ? $this->mapToDomainEntity(doctrineProjection: $doctrineProjection) : null;
    }

    /**
     * Update an existing entity.
     *
     * @param BankAccountMonthlyProjection $projection
     */
    public function update(BankAccountMonthlyProjection $projection): void
    {
        $dql = 'UPDATE Infrastructure\Symfony\Entity\BankAccountMonthlyProjection p
                SET p.balance = :balance,
                    p.totalDeposits = :totalDeposits,
                    p.totalWithdrawals = :totalWithdrawals
                WHERE p.accountId = :accountId AND p.month = :month';

        $query = $this->entityManager->createQuery($dql)
            ->setParameter('balance', $projection->balance)
            ->setParameter('totalDeposits', $projection->totalDeposits)
            ->setParameter('totalWithdrawals', $projection->totalWithdrawals)
            ->setParameter('accountId', $projection->accountId)
            ->setParameter('month', $projection->month);

        $query->execute();
    }


    public function exists(string $accountId, string $month): bool
    {
        $sql = 'SELECT COUNT(*) FROM bank_account_monthly_projection WHERE account_id = :accountId AND month = :month';

        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue('accountId', $accountId);
        $stmt->bindValue('month', $month);

        $result = $stmt->executeQuery()->fetchOne();

        return (int) $result > 0;
    }

    /**
     * Mappe une entité de domaine à une entité Doctrine.
     *
     * @param BankAccountMonthlyProjection $projection
     * @return DoctrineBankAccountMonthlyProjection
     */
    private function mapToDoctrineEntity(BankAccountMonthlyProjection $projection): DoctrineBankAccountMonthlyProjection
    {
        $doctrineProjection = new DoctrineBankAccountMonthlyProjection();
        $doctrineProjection->setAccountId($projection->accountId);
        $doctrineProjection->setMonth($projection->month);
        $doctrineProjection->setBalance($projection->balance);
        $doctrineProjection->setTotalDeposits($projection->totalDeposits);
        $doctrineProjection->setCreatedAt(new \DateTimeImmutable($projection->createdAt->format('Y-m-d H:i:s')));
        $doctrineProjection->setUpdatedAt(new \DateTimeImmutable($projection->updatedAt->format('Y-m-d H:i:s')));

        $doctrineProjection->setTotalWithdrawals($projection->totalWithdrawals);


        return $doctrineProjection;
    }

    /**
     * Mappe une entité Doctrine à une entité de domaine.
     *
     * @param DoctrineBankAccountMonthlyProjection $doctrineProjection
     * @return BankAccountMonthlyProjection
     */
    private function mapToDomainEntity(DoctrineBankAccountMonthlyProjection $doctrineProjection): BankAccountMonthlyProjection
    {
        return new BankAccountMonthlyProjection(
            accountId: $doctrineProjection->getAccountId(),
            month: $doctrineProjection->getMonth(),
            balance: $doctrineProjection->getBalance(),
            totalDeposits: $doctrineProjection->getTotalDeposits(),
            totalWithdrawals: $doctrineProjection->getTotalWithdrawals()
        );
    }
}
