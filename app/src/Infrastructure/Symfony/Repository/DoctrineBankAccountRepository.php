<?php
namespace Infrastructure\Symfony\Repository;

use Domain\Bank\BankAccount;
use Ramsey\Uuid\UuidInterface;
use Domain\Bank\BankAccountRepository;
use Doctrine\Persistence\ManagerRegistry;
use Infrastructure\Symfony\Repository\EventStoreRepository;


class DoctrineBankAccountRepository implements BankAccountRepository
{


    public function __construct(private ManagerRegistry $registry, private EventStoreRepository $eventStoreRepository)
    {
    }

    public function get(UuidInterface $id): ?BankAccount
    {
        $events = $this->eventStoreRepository->findByAggregateId($id->toString());
        if (empty($events))
            return null;

        $bankAccount = new BankAccount(id: $id);

        foreach ($events as $event)
            $bankAccount->apply(event: unserialize($event->getPayload()));

        return $bankAccount;
    }

}