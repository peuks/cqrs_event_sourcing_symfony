<?php

namespace Infrastructure\Symfony\Entity;

use Doctrine\ORM\Mapping as ORM;
use Infrastructure\Symfony\Repository\BankAccountMonthlyProjectionRepository;

#[ORM\Entity(repositoryClass: BankAccountMonthlyProjectionRepository::class)]
class BankAccountMonthlyProjection
{
    #[ORM\Id]
    #[ORM\Column(type: 'string', length: 36)]
    private string $accountId;

    #[ORM\Id]
    #[ORM\Column(type: 'string', length: 7)]
    private string $month;

    #[ORM\Column(type: 'float')]
    private float $balance = 0.0;

    #[ORM\Column(type: 'float')]
    private float $totalDeposits = 0.0;

    #[ORM\Column(type: 'float')]
    private float $totalWithdrawals = 0.0;

    #[ORM\Column]
    private ?\DateTimeImmutable $updatedAt = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $createdAt = null;

    public function getAccountId(): string
    {
        return $this->accountId;
    }

    public function getMonth(): string
    {
        return $this->month;
    }

    public function getBalance(): float
    {
        return $this->balance;
    }

    public function getTotalDeposits(): float
    {
        return $this->totalDeposits;
    }

    public function getTotalWithdrawals(): float
    {
        return $this->totalWithdrawals;
    }

    // Setters
    public function setAccountId(string $accountId): void
    {
        $this->accountId = $accountId;
    }

    public function setMonth(string $month): void
    {
        $this->month = $month;
    }

    public function setBalance(float $balance): void
    {
        $this->balance = $balance;
    }

    public function setTotalDeposits(float $totalDeposits): void
    {
        $this->totalDeposits = $totalDeposits;
    }

    public function setTotalWithdrawals(float $totalWithdrawals): void
    {
        $this->totalWithdrawals = $totalWithdrawals;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
