<?php

namespace Infrastructure\Symfony\CommandBus;

use Application\Shared\Contracts\Command;

final class UniversalCommandBusHandler
{

    public function __construct(private FacadeCommandBus $commandBus)
    {
    }

    public function __invoke(Command $command): void
    {
        $this->commandBus->dispatch(command: $command);
    }
}
