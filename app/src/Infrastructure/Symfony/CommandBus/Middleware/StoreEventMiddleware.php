<?php

namespace Infrastructure\Symfony\CommandBus\Middleware;

use ReflectionClass;
use Application\Shared\Contracts\Event;
use Application\Shared\Contracts\Command;
use Application\Shared\Attributes\StorableEvent;
use Application\Shared\Contracts\CommandResponse;
use Infrastructure\Symfony\Repository\EventStoreRepository;
use Application\Services\CommandBus\Middleware\ICommandBusMiddleware;

/**
 * Middleware that stores events in the database if they have the StorableEvent attribute.
 */
class StoreEventMiddleware implements ICommandBusMiddleware
{
    public function __construct(
        private EventStoreRepository $eventStoreRepository,
        private ICommandBusMiddleware $next
    ) {
    }

    public function dispatch(Command $command): CommandResponse
    {
        $response = $this->next->dispatch($command);
        foreach ($response->events() as $event)
            if ($this->shouldStoreEvent($event))
                $this->storeEvent($event);
        return $response;
    }

    private function shouldStoreEvent(Event $event): bool
    {
        $reflection = new ReflectionClass($event);
        return !empty($reflection->getAttributes(StorableEvent::class));
    }

    private function storeEvent(Event $event): void
    {
        $this->eventStoreRepository->store($event);
    }
}
