<?php

namespace Infrastructure\Symfony\CommandBus\Middleware;

use Application\Services\CommandBus\Middleware\ICommandBusMiddleware;
use Application\Shared\Contracts\Command;
use Application\Shared\Contracts\CommandResponse;
use Psr\Log\LoggerInterface;



/**
 * Middleware that logs the execution time of command dispatching within a command bus.
 * 
 * It measures the time before and afterApplication\Shared\Command dispatching the command to the next middleware
 * and logs this duration along with the command class name. This can be useful for 
 * monitoring command execution and identifying performance bottlenecks.
 *
 * @package Application\Shared\CommandBus\Middleware
 */
class LoggerMiddleware implements ICommandBusMiddleware
{
    public function __construct(
        private ICommandBusMiddleware $next,
        private LoggerInterface $logger
    ) {
    }

    public function dispatch(Command $command): CommandResponse
    {
        $startTime = microtime(true);

        $response = $this->next->dispatch(command: $command);

        $endTime = microtime(true);

        $elapsed = $endTime - $startTime;

        $this->logger->info('Command dispatched.', [
            'command' => get_class($command),
            'duration' => $elapsed,
            'unit' => 'seconds'
        ]);

        return $response;
    }
}
