<?php
declare(strict_types=1);
namespace Infrastructure\Symfony\CommandBus\Middleware;

use Application\Shared\Contracts\Command;
use Symfony\Component\Messenger\MessageBusInterface;
use Application\Subscription\Command\CommandResponse;
use Application\Services\CommandBus\Middleware\ICommandBusMiddleware;

class AsyncMiddleware implements ICommandBusMiddleware
{
    public function __construct(
        private MessageBusInterface $bus,
        private ICommandBusMiddleware $next
    ) {
    }

    public function dispatch(Command $command): CommandResponse
    {
        $this->bus->dispatch($command);

        return CommandResponse::withValue(value: "Commande en cours de traitement", events: []);
    }
}