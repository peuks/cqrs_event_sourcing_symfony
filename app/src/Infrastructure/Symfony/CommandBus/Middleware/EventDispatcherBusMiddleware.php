<?php
declare(strict_types=1);
namespace Infrastructure\Symfony\CommandBus\Middleware;

use Application\Shared\Contracts\Command;
use Application\Services\EventBus\IEventBus;
use Application\Shared\Contracts\CommandResponse;
use Application\Services\CommandBus\Middleware\ICommandBusMiddleware;

/**
 * EventDispatcherBusMiddleware is responsible for dispatching events that are 
 * raised during the handling of a command. It ensures that after a command is handled, 
 * any resulting events are passed to the event bus for further processing.
 */
class EventDispatcherBusMiddleware implements ICommandBusMiddleware
{
    public function __construct(private ICommandBusMiddleware $next, private IEventBus $eventBus)
    {
    }

    public function dispatch(Command $command): CommandResponse
    {
        $commandResponse = $this->next->dispatch($command);
        if ($commandResponse->hasEvents())
            foreach ($commandResponse->events() as $event)
                $this->eventBus->dispatch($event);

        return $commandResponse;
    }
}
