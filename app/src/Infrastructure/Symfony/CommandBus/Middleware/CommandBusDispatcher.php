<?php
declare(strict_types=1);

namespace Infrastructure\Symfony\CommandBus\Middleware;

use LogicException;
use ReflectionClass;
use Application\Shared\Contracts\Command;
use Application\Shared\Attributes\HandlesCommand;
use Application\Shared\Contracts\CommandResponse;
use Application\Services\Handlers\CommandHandlers\ICommandHandler;
use Application\Services\CommandBus\Middleware\ICommandBusMiddleware;


/**
 * Dispatches command objects to their corresponding handlers.
 *
 * This dispatcher is responsible for mapping command objects to their respective handlers
 * and then invoking the handler. If a command does not have an associated handler, a
 * LogicException is thrown.
 */
class CommandBusDispatcher implements ICommandBusMiddleware
{
    private array $handlers = [];

    public function __construct(iterable $handlers)
    {
        foreach ($handlers as $handler)
            $this->registerHandler($handler);
    }

    private function registerHandler($handler): void
    {
        $refClass = new ReflectionClass($handler);
        $attributes = $refClass->getAttributes(HandlesCommand::class);


        foreach ($attributes as $attribute) {
            $commandType = $attribute->newInstance()->commandType;
            if (isset($this->handlers[$commandType])) {
                $existingHandlerClass = $this->handlers[$commandType]::class;
                $newHandlerClass = $refClass->getName();
                throw new LogicException("Duplicate command handler registered for command type {$commandType}: Existing handler is {$existingHandlerClass}, attempting to register {$newHandlerClass}.");
            }
            $this->handlers[$commandType] = $handler;
        }
    }
    /**
     * Dispatches a command to the appropriate handler.
     *
     * @param Command $command The command to dispatch.
     * @return CommandResponse The response from the command handler.
     * @throws LogicException If no handler is found for the given command.
     */
    public function dispatch(Command $command): CommandResponse
    {
        $commandClass = $command::class;
        /**
         * @var ICommandHandler $handler   
         */
        $handler = $this->handlers[$commandClass] ?? $this->handleMissingHandler($commandClass);
        return $handler->handle(command: $command);
    }

    /**
     * Handles the case when no command handler is found for the given command class.
     *
     * @param string $commandClass The class name of the command that lacks a handler.
     * @throws LogicException Always thrown in the current implementation to indicate an error.
     */
    private function handleMissingHandler(string $commandClass): void
    {
        throw new LogicException("Handler for command {$commandClass} not found.");
    }
}
