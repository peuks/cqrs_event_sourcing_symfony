<?php
declare(strict_types=1);

namespace Infrastructure\Symfony\CommandBus;

use Psr\Log\LoggerInterface;
use Application\Services\EventBus\IEventBus;
use Application\Services\CommandBus\ICommandBus;
use Symfony\Component\Messenger\MessageBusInterface;
use Infrastructure\Symfony\Repository\EventStoreRepository;
use Infrastructure\Symfony\CommandBus\Middleware\AsyncMiddleware;
use Infrastructure\Symfony\CommandBus\Middleware\LoggerMiddleware;
use Infrastructure\Symfony\CommandBus\Middleware\CommandBusDispatcher;
use Infrastructure\Symfony\CommandBus\Middleware\StoreEventMiddleware;
use Infrastructure\Symfony\CommandBus\Middleware\EventDispatcherBusMiddleware;

/**
 * Factory for creating a Command Bus.
 *
 * This class sets up the Command Bus with necessary middleware, particularly
 * with logging capabilities. It ensures that each command processed by the bus
 * is logged for auditing and debugging purposes. The Command Bus is composed of
 * a dispatcher that is responsible for routing commands to their respective handlers.
 * LoggerMiddleware is used to wrap the dispatcher for logging each command's handling.
 */
class CommandBusFactory
{
    public function __construct(
        private EventStoreRepository $eventStoreRepository,
        private MessageBusInterface $messenger,
        private LoggerInterface $logger,
        private IEventBus $eventBus,
        private iterable $handlers,
    ) {

    }

    /**
     * Builds and returns a synchronous Command Bus configured without the AsyncMiddleware.
     *
     * @return ICommandBus
     */
    public function createSyncBus(): ICommandBus
    {
        return new CommandBus(
            new LoggerMiddleware(
                logger: $this->logger,
                next: new StoreEventMiddleware(
                    eventStoreRepository: $this->eventStoreRepository,
                    next: new EventDispatcherBusMiddleware(
                        next: new CommandBusDispatcher($this->handlers),
                        eventBus: $this->eventBus
                    )
                )
            )
        );
    }

    /**
     * Builds and returns an asynchronous Command Bus configured with AsyncMiddleware.
     *
     * @return ICommandBus
     */
    public function createAsyncBus(): ICommandBus
    {
        return new CommandBus(
            new LoggerMiddleware(
                logger: $this->logger,
                next: new AsyncMiddleware(
                    bus: $this->messenger,
                    next: new EventDispatcherBusMiddleware(
                        next: new CommandBusDispatcher($this->handlers),
                        eventBus: $this->eventBus
                    )
                )
            )
        );
    }
}
