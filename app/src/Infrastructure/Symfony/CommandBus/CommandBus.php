<?php
namespace Infrastructure\Symfony\CommandBus;

use Application\Shared\Contracts\Command;
use Application\Services\CommandBus\ICommandBus;
use Application\Shared\Contracts\CommandResponse;
use Application\Services\CommandBus\Middleware\ICommandBusMiddleware;

class CommandBus implements ICommandBus
{

    public function __construct(private ICommandBusMiddleware $middleware)
    {
    }

    public function dispatch(Command $command): CommandResponse
    {
        return $this->middleware->dispatch($command);
    }
}
