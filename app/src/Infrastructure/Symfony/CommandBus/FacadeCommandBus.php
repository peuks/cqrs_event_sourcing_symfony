<?
namespace Infrastructure\Symfony\CommandBus;

use Application\Shared\Contracts\Command;


class FacadeCommandBus
{
    public function __construct(private CommandBusFactory $commandBusFactory)
    {
    }

    public function dispatch(Command $command)
    {
        return $this->commandBusFactory->createSyncBus()->dispatch(command: $command);
    }

    public function async()
    {
        return $this->commandBusFactory->createAsyncBus();
    }
}