<?php

namespace Infrastructure\Symfony\Controller;

use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Infrastructure\Symfony\Dto\DepositMoneyRequest;
use Infrastructure\Symfony\CommandBus\FacadeCommandBus;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Application\Bank\Command\DepositMoney\DepositMoneyCommand;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DepositMoneyController extends AbstractController
{
    public function __construct(private FacadeCommandBus $commandBus)
    {
    }

    #[Route('/bank/deposit', name: 'app_create_deposite', methods: ['POST'])]
    public function __invoke(#[MapRequestPayload()] DepositMoneyRequest $request): JsonResponse
    {
        $response = $this->commandBus->dispatch(command: new DepositMoneyCommand(accountId: $request->accountId, amount: $request->amount));

        return $this->json([
            'data' => ['id' => $response->value],
        ]);
    }
}


