<?php

namespace Infrastructure\Symfony\Controller;

use Application\Bank\Query\GetBankAccount\GetBankAccountQuery;
use Application\Services\QueryBus\IQueryBus;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class GetBankAccountController extends AbstractController
{
    public function __construct(private IQueryBus $queryBus)
    {
    }

    #[Route('/bank/{id}', name: 'app_get_bank_account', methods: ['GET'])]
    public function __invoke(string $id): JsonResponse
    {

        return $this->json(
            data: $this->queryBus->dispatch(
                new GetBankAccountQuery(id: $id)
            )->value->snapshot(),
            status: Response::HTTP_OK
        );
    }
}
