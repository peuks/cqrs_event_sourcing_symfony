<?php

namespace Infrastructure\Symfony\Controller;

use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Infrastructure\Symfony\Dto\CreateAccountRequest;
use Infrastructure\Symfony\CommandBus\FacadeCommandBus;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Application\Bank\Command\CreateAccount\CreateAccountCommand;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CreateAccountController extends AbstractController
{
    public function __construct(private FacadeCommandBus $commandBus)
    {
    }
    #[Route('/bank/create', name: 'app_create_account', methods: ['POST'], defaults: ['_format' => 'json'])]
    public function __invoke(#[MapRequestPayload] CreateAccountRequest $createAccountRequest): JsonResponse
    {
        $response = $this->commandBus->dispatch(command: new CreateAccountCommand($createAccountRequest->accountType));

        return $this->json([
            'data' => ['id' => $response->value],
        ]);
    }
}
