<?php

namespace Infrastructure\Symfony\Controller;

use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Infrastructure\Symfony\Dto\WithdrawMoneyRequest;
use Infrastructure\Symfony\CommandBus\FacadeCommandBus;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Application\Bank\Command\WithdrawMoney\WithdrawMoneyCommand;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class WithdrawMoneyController extends AbstractController
{
    public function __construct(private FacadeCommandBus $commandBus)
    {
    }

    #[Route('/bank/withdraw', name: 'app_create_withdraw', methods: ['POST'], defaults: ['_format' => 'json'])]

    public function __invoke(#[MapRequestPayload()] WithdrawMoneyRequest $request): JsonResponse
    {
        $test = $this->commandBus->dispatch(command: new WithdrawMoneyCommand(accountId: $request->accountId, amount: $request->amount));
        return $this->json([
            'data' => ['id' => $test->value],
        ]);
    }
}


