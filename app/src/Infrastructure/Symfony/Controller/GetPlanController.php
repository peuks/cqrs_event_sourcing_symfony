<?php
declare(strict_types=1);

namespace Infrastructure\Symfony\Controller;

use Ramsey\Uuid\Uuid;
use Application\Services\QueryBus\IQueryBus;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Application\Subscription\Query\FindActiveSubscriptionPlansQuery;

class GetPlanController extends AbstractController
{
    public function __construct(private IQueryBus $queryBus)
    {
    }

    #[Route('/get/plan/{id}', name: 'app_get_plan')]
    public function __invoke(Request $request): JsonResponse
    {
        return $this->json(
            data: $this->queryBus->dispatch(
                query: new FindActiveSubscriptionPlansQuery(
                    id: Uuid::fromString($request->get('id'))
                )
            ),
            status: Response::HTTP_OK
        );
    }
}