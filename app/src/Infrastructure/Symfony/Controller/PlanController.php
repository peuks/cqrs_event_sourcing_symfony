<?php

namespace Infrastructure\Symfony\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Infrastructure\Symfony\CommandBus\FacadeCommandBus;
use Infrastructure\Symfony\Dto\CreateSubscriptionPlanRequest;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Application\Subscription\Command\CreateSubscriptionPlanCommand;

class PlanController extends AbstractController
{
    public function __construct(private FacadeCommandBus $commandBus)
    {
    }


    #[Route('/plan', name: 'app_plan', format: 'json', methods: ['POST']),
    ]
    public function __invoke(
        #[MapRequestPayload] CreateSubscriptionPlanRequest $request
    ): Response {
        $command = new CreateSubscriptionPlanCommand(
            price: (float) $request->price,
            name: $request->name,
            annualDiscount: (float) $request->annualDiscount,
            expirationDate: $request->annualDiscount,
            includedActivities: $request->includedActivities
        );
        $this->commandBus->async()->dispatch(command: $command);
        return new Response(status: Response::HTTP_OK);
    }
}
