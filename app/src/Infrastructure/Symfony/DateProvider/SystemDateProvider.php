<?php
declare(strict_types=1);
namespace Infrastructure\Symfony\DateProvider;

use Application\Shared\Contracts\DateProvider;

class SystemDateProvider implements DateProvider
{
    public function currentDateTime(): \DateTime
    {
        return new \DateTime();
    }
}
