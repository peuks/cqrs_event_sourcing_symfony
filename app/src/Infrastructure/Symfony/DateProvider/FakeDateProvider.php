<?php
declare(strict_types=1);
namespace Infrastructure\Symfony\DateProvider;

use Application\Shared\Contracts\DateProvider;

class FakeDateProvider implements DateProvider
{

    public function __construct(private \DateTime $fakeDate)
    {
    }

    public function currentDateTime(): \DateTime
    {
        return $this->fakeDate;
    }
}
