<?php
declare(strict_types=1);
namespace Infrastructure\Symfony\EventBus;

use ReflectionClass;
use Application\Shared\Contracts\Event;
use Application\Services\EventBus\IEventBus;
use Application\Shared\Attributes\HandlesEvent;
use Application\Services\Handlers\EventHandlers\IEventHandler;



class EventBus implements IEventBus
{
    public function __construct(private iterable $handlers)
    {
    }

    public function dispatch(Event $event): void
    {
        /** @var IEventHandler $handler */
        foreach ($this->handlers as $handler) {
            $reflection = new ReflectionClass(objectOrClass: $handler);
            $attributes = $reflection->getAttributes(HandlesEvent::class);
            foreach ($attributes as $attribute)

                if (in_array($event::class, $attribute->newInstance()->events))
                    $handler->handle(event: $event);
        }
    }
}
