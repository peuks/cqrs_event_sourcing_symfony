<?php
declare(strict_types=1);

namespace Domain\Subscription;


readonly class Member
{
    public function __construct(
        public MemberId $memberId,
        public string $firstName,
        public string $email,
        public \DateTime $joinedAt
    ) {
    }
}
