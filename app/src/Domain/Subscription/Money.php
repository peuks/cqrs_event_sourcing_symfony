<?php
declare(strict_types=1);
namespace Domain\Subscription;

readonly class Money
{
    private function __construct(
        public float $amount,
        public string $currency
    ) {
        if ($amount < 0)
            throw new \InvalidArgumentException('Amount cannot be negative.');
    }

    public static function EURO(float $amount): self
    {
        return new self(amount: $amount, currency: 'EUR');
    }

}
