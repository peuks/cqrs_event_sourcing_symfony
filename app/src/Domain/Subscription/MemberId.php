<?php
declare(strict_types=1);
namespace Domain\Subscription;


class MemberId
{

    public function __construct(private string $id)
    {
    }

    public function __toString(): string
    {
        return $this->id;
    }
}
