<?
declare(strict_types=1);
namespace Domain\Subscription;

use Ramsey\Uuid\UuidInterface;


interface SubscriptionPlanRepository
{

    public function get(UuidInterface $id): ?SubscriptionPlan;
    public function add(SubscriptionPlan $subscriptionPlan): void;
    public function delete(UuidInterface $id): void;

    /**
     * @return SubscriptionPlan[]
     */
    public function getAll(): array;

    /**
     * @return SubscriptionPlan[]
     */
    public function findActivePlan(UuidInterface $id): array;
}