<?php
namespace Domain\Subscription\Events\Handlers;

use Domain\Subscription\MemberId;
use Symfony\Component\Mime\Email;
use Application\Shared\Contracts\Event;
use Domain\Subscription\MemberRepository;
use Domain\Subscription\Events\MemberJoined;
use Symfony\Component\Mailer\MailerInterface;
use Application\Shared\Attributes\HandlesEvent;
use Application\Services\Handlers\EventHandlers\IEventHandler;

#[HandlesEvent(events: [MemberJoined::class])]
class SendWelcomeMailOnMemberJoined implements IEventHandler
{
    public function __construct(
        private MailerInterface $mailer,
        private MemberRepository $memberRepository
    ) {
    }
    /** @param MemberJoined $event */
    public function handle(Event $event): void
    {
        $member = $this->memberRepository->get(new MemberId(id: $event->newMemberId->__toString()));
        $message = (new Email())
            ->from('welcome@sportland.com')
            ->to($member->email)
            ->subject('Welcome to SportLand!')
            ->text("Hello {$member->firstName}, welcome in SportLand. We hope you'll enjoy your subscription.");

        $this->mailer->send($message);
    }
}