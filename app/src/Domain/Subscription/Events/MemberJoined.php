<?php
declare(strict_types=1);
namespace Domain\Subscription\Events;

use Ramsey\Uuid\UuidInterface;
use Application\Shared\Contracts\Event;

readonly class MemberJoined implements Event
{
    public function __construct(
        public UuidInterface $newMemberId,
        public UuidInterface $chosenPlanId
    ) {
    }
}