<?
declare(strict_types=1);
namespace Domain\Subscription;

use DateTime;

class SubscriptionPlan
{

    public function __construct(
        private string $name,
        private Money $price,
        private float $annualDiscount,
        private DateTime $expirationDate,
        private array $includedActivities,
        private ?string $id = null,
        private ?DateTime $createdAt = new DateTime(),
        private ?DateTime $updatedAt = new DateTime(),
    ) {
    }

    private function setAnnualDiscount(float $annualDiscount): void
    {
        if ($annualDiscount < 0.0 || $annualDiscount > 1.0)
            throw new \InvalidArgumentException('Annual discount must be between 0.0 and 1.0.');

        $this->annualDiscount = $annualDiscount;
    }
    public function snapshot(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'price' => [
                'amount' => $this->price->amount,
                'currency' => $this->price->currency
            ],
            'annualDiscount' => $this->annualDiscount,
            'expirationDate' => $this->expirationDate->format(DateTime::ATOM),
            'includedActivities' => $this->includedActivities,
            'createdAt' => $this->createdAt->format(DateTime::ATOM),
            'updatedAt' => $this->updatedAt->format(DateTime::ATOM)
        ];
    }
}