<?
declare(strict_types=1);
namespace Domain\Subscription;


interface MemberRepository
{
    public function get(MemberId $memberId): Member;
}