<?php

declare(strict_types=1);

namespace Domain\Bank;

use Ramsey\Uuid\UuidInterface;
use Domain\Bank\Events\MoneyDeposited;
use Domain\Bank\Events\MoneyWithdraw;
use Domain\Bank\Events\BankAccountCreated;
use Application\Shared\Contracts\AggregateRoot;
use Domain\Bank\Exception\InsufficientFundsException;

class BankAccount implements AggregateRoot
{
    use BankAccountEventApplier;

    private float $balance = 0.0;
    private string $accountType = '';
    private array $events = [];

    public function __construct(public readonly UuidInterface $id)
    {
    }

    public function create(string $accountType): array
    {
        if (empty($accountType)) {
            throw new \InvalidArgumentException('Account type must be specified');
        }

        $event = new BankAccountCreated(
            id: $this->id->toString(),
            accountType: $accountType
        );
        $this->apply($event);
        $this->events[] = $event;
        return $this->events;
    }
    public function deposit(float $amount): array
    {
        if ($amount <= 0)
            throw new \InvalidArgumentException('Deposit amount must be positive');

        $event = new MoneyDeposited(id: $this->id->toString(), amount: $amount);
        $this->apply(event: $event);
        $this->events[] = $event;
        return $this->events;
    }

    public function withdraw(float $amount): array
    {
        if ($this->balance < $amount)
            throw new InsufficientFundsException();

        $event = new MoneyWithdraw(id: $this->id->toString(), amount: $amount);
        $this->apply(event: $event);
        $this->events[] = $event;
        return $this->events;
    }

    public function snapshot(): array
    {
        return [
            'id' => $this->id->toString(),
            'balance' => $this->balance,
            'accountType' => $this->accountType
        ];
    }
}
