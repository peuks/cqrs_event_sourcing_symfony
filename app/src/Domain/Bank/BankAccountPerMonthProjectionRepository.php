<?php
declare(strict_types=1);
namespace Domain\Bank;

use Domain\Bank\Projections\BankAccountMonthlyProjection;


interface BankAccountPerMonthProjectionRepository
{
    public function save(BankAccountMonthlyProjection $projection): void;
    public function get(string $accountId, string $month): ?BankAccountMonthlyProjection;
    public function exists(string $accountId, string $month): bool;
    public function update(BankAccountMonthlyProjection $projection): void;
}
