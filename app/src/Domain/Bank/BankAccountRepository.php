<?
namespace Domain\Bank;

use Ramsey\Uuid\UuidInterface;

interface BankAccountRepository
{
    public function get(UuidInterface $id): ?BankAccount;
}
