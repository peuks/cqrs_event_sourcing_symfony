<?php

declare(strict_types=1);

namespace Domain\Bank;

use Domain\Bank\Events\MoneyDeposited;
use Domain\Bank\Events\MoneyWithdraw;
use Application\Shared\Contracts\Event;
use Domain\Bank\Events\BankAccountCreated;

trait BankAccountEventApplier
{
    public function apply(Event $event): void
    {
        $method = 'apply' . (new \ReflectionClass($event))->getShortName();
        if (method_exists($this, $method))
            $this->$method($event);

    }

    private function applyMoneyDeposited(MoneyDeposited $event): void
    {
        $this->balance += $event->amount;
    }

    private function applyMoneyWithdraw(MoneyWithdraw $event): void
    {
        $this->balance -= $event->amount;
    }

    private function applyBankAccountCreated(BankAccountCreated $event): void
    {
        $this->accountType = $event->accountType;
    }

}
