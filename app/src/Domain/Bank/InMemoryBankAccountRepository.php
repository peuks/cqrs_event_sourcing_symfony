<?php
declare(strict_types=1);

namespace Domain\Bank;

use Domain\Bank\BankAccount;
use Ramsey\Uuid\UuidInterface;
use Domain\Bank\BankAccountRepository;

class InMemoryBankAccountRepository implements BankAccountRepository
{
    private array $accounts = [];

    public function get(UuidInterface $id): ?BankAccount
    {
        $idString = $id->toString();
        if (!isset($this->accounts[$idString]))
            return null;

        return $this->accounts[$idString];
    }

    public function save(BankAccount $account): void
    {
        $this->accounts[$account->id->toString()] = $account;
    }
}
