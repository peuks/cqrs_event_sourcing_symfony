<?

namespace Domain\Bank\Exception;

use Exception;

class InsufficientFundsException extends Exception
{
    public function __construct($message = "Insufficient funds")
    {
        parent::__construct($message);
    }
}
