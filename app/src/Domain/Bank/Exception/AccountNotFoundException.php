<?php
namespace Domain\Bank\Exception;

use Exception;

class AccountNotFoundException extends Exception
{
    public function __construct($message = "Account not found")
    {
        parent::__construct($message);
    }
}
