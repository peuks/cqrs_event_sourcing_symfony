<?
namespace Domain\Bank\Events;

use Application\Shared\Contracts\Event;
use Application\Shared\Attributes\StorableEvent;

#[StorableEvent]
readonly class MoneyWithdraw implements Event
{
    public function __construct(
        public string $id,
        public float $amount
    ) {
    }
}