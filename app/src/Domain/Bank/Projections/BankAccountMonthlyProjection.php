<?php

declare(strict_types=1);

namespace Domain\Bank\Projections;

class BankAccountMonthlyProjection
{
    public function __construct(
        public readonly string $accountId,
        public readonly string $month,
        public readonly \DateTime $createdAt,
        public readonly \DateTime $updatedAt,
        public readonly float $balance = 0.0,
        public readonly float $totalDeposits = 0.0,
        public readonly float $totalWithdrawals = 0.0
    ) {
    }

    public static function create(
        string $accountId,
        string $month,
        \DateTime $createdAt,
        \DateTime $updatedAt,
        float $balance = 0.0,
        float $totalDeposits = 0.0,
        float $totalWithdrawals = 0.0
    ): self {
        return new self(
            accountId: $accountId,
            month: $month,
            createdAt: $createdAt,
            updatedAt: $updatedAt,
            balance: $balance,
            totalDeposits: $totalDeposits,
            totalWithdrawals: $totalWithdrawals
        );
    }

    public function toArray(): array
    {
        return [
            'accountId' => $this->accountId,
            'month' => $this->month,
            'balance' => $this->balance,
            'totalDeposits' => $this->totalDeposits,
            'totalWithdrawals' => $this->totalWithdrawals,
            'createdAt' => $this->createdAt,
            'updatedAt' => $this->updatedAt,
        ];
    }

    public static function fromArray(array $data): self
    {
        return new self(
            accountId: $data['accountId'],
            month: $data['month'],
            createdAt: $data['createdAt'],
            updatedAt: $data['updatedAt'],
            balance: $data['balance'],
            totalDeposits: $data['totalDeposits'],
            totalWithdrawals: $data['totalWithdrawals']
        );
    }
}
